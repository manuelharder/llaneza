<?php

class Button extends FormControl {
		

	protected $boolSubmitBtn;
		
	protected $boolAjax;
		
	/**
	 * Creates new Button (Submit Button) Object
	 * @param objParent (Parent Object, probably a form object)
	 * @param strName - (value for the name attr)
	 * @param boolAjax - (will the button trigger an ajax call)
	 * @param boolSubmitBtn - (false for '< button >')   
	 */
	public function __construct($objParent, $strName, $boolAjax = false, $boolSubmitBtn = true) {
		
		parent::__construct($objParent, $strName);
		
		$this->boolAjax = Type::check($boolAjax, "boolean");
		
		$this->boolSubmitBtn = Type::check($boolSubmitBtn, "boolean");
	}
	
	
	public function render($display = true, $strRender = "") {		
				
		if($this->boolSubmitBtn) {
			
			$ajaxCall = $this->boolAjax ? "onclick='return false;'" : "";
						
			$strRender = sprintf('<input type="submit" name="%s" value="%s" %s %s />',
										$this->strName, 
										$this->strValue,
										$this->setAttributes(),
										$ajaxCall);
		}
		else {
			
			$strRender = sprintf('<button type="button" name="%s" %s >%s</button>',
										$this->strName, 
										$this->setAttributes(),
										$this->strValue);
		}
		
		return parent::render($display, $strRender);																
	}
	
	
	
	public function __set($strName, $value) {

		switch ($strName) {
	
			case "SubmitBtn": 
				try {
					$this->boolSubmitBtn = Type::check($value, "boolean");
					break;
				} catch (Exception $e) {
					throw $e;
				}
		
			default: parent::__set($strName, $value);
		
		}
	}
	
	public function __get($strName) {
		
			switch ($strName) {
					
					case 'SubmitBtn':		return $this->boolSubmitBtn;	break;
					default:					return parent::__get($strName);	
			}
	}
	
	
}
