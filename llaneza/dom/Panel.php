<?php

abstract class Panel extends DOMControl {
		

	protected $panelHTML;
	protected $strTemplateName;
	
	protected $boolRenderInClass;
			
	
	public function __construct($objParent, $renderInClass = false) {
		
		parent::__construct($objParent);
		
		$this->boolRenderInClass = Type::check($renderInClass, "boolean");
		
		static::getHTML();		
	}
	
	abstract public function getHTML();
	
	
	public function render($display = true, $strRender = "") {		
											
		try {
			
			if($this->boolRenderInClass) $this->panelHTML = static::rendered();
			
			else { 
				
				ob_start();
				
				if (!$this->strTemplateName) $this->strTemplateName = strtolower(get_class($this));				
							
				include_once(__TPL__ . 'panel/' . $this->strTemplateName . '.tpl.php');
				
				$this->panelHTML = ob_get_contents();
			
				ob_end_clean();
			}
	    } 
	    catch (Exception $e) {
	   		throw $e;
     	}
		
		$strRender = sprintf('<div%s>%s</div>',
										$this->setAttributes(),
										$this->panelHTML);
	
		return parent::render($display, $strRender);																
	}
	
	

	
	public function __set($strName, $value) {
		
			switch ($strName) {

					default: parent::__set($strName, $value);
			}
	}
	
	public function __get($strName) {
		
			switch ($strName) {

					case 'RenderInClass':
						return $this->boolRenderInClass;
						break;	
					case 'strTemplateName':
						return $this->strTemplateName;	
						break;
	
						
					default: 
						return parent::__get($strName);
			}
	}
	
	
}

?>