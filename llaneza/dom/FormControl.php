<?php

abstract class FormControl extends DOMControl {
		

	protected $strName;
	protected $strValue;
	protected $boolRadioBtn = false;
	
	protected static $arrNames = array();
	

	public function __construct($objParent, $strName, $radioBtn = false) {
		
		parent::__construct($objParent);
		
		$this->boolRadioBtn = $radioBtn;
		
		$this->checkName($strName);
	}
	
	
	protected function checkName($strName) {
		
		if(empty($strName)) 							 throw new MyException("NO_FORM_NAME");
		if(in_array($strName, self::$arrNames) && !$this->boolRadioBtn) throw new MyException("NAME_NOT_UNIQUE");
		
		if ($strName == "EMPTY") $strName = "";
		
		$this->strName    = $strName;
		self::$arrNames[] = $strName;
	}

	
	public function setValue($strValue) {
		
		$this->strValue = $strValue;
	}
	
	public function setAjaxPost($arrValues) {
		
		if (is_a($this, "CheckBox")) {

			if (!isset($arrValues[$this->strName])) {
				
				$this->boolChecked = false;
				$this->strSubmited = "";
				return;
			}
			
			if ($arrValues[$this->strName] == "")	{
				
				$this->strSubmited = "";
				$this->boolChecked = false; 
			}
			else {
				$this->strSubmited = $arrValues[$this->strName];
				$this->boolChecked = true;
			}
		}
		
		elseif (is_a($this, "RadioButton")) {
			 
			if (!isset($arrValues[$this->strName])) {
				
				$this->boolChecked = false;
				return;
			}
			else {
				$this->strSubmited = $arrValues[$this->strName];				
			}
			
			if ($this->strValue == "") $this->strValue = $this->strText;
			
			if ($arrValues[$this->strName] == $this->strValue)	{
				
				$this->boolChecked = true;
			}
			else $this->boolChecked = false;
		}
		
		elseif (is_a($this, "SelectBox")) {
                    
			if (isset($arrValues[$this->strName])) $this->strValue = $arrValues[$this->strName];
		}
		
		else {
			if (isset($arrValues[$this->strName])) $this->strValue = $arrValues[$this->strName];
		}
	}
	
	
	public function setName($strName) {
		
		$this->checkName($strName);
	}
	
	
	public function __set($strName, $value) {

		switch ($strName) {
	
			case "Name": 
					$this->strName = $value;
					break;
			case "Value": 
					$this->strValue = $value;
					break;
					
			default: parent::__set($strName, $value);
		}
	}
	
	
	public function __get($strName) {
		
			switch ($strName) {
					
					case 'Name':
						return $this->strName;
					case 'Value':
						return $this->strValue;
					
					default: 
						return parent::__get($strName);
		}
	}
	
	
}
