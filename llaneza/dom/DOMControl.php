<?php

abstract class DOMControl {
	
	
	protected $arrCssClasses 	= array();
	protected $strId;
	protected $arrStyles		= array();
	protected $arrCustomAttr	= array();
			
	protected $boolAjaxLLCallable = false;
	
	protected $objParent;
	
	protected $boolHidden = false;
	protected $boolShow = false;
	
	protected static $arrId		= array();
	
	public $LLCallables = array();	
	
	
	public function __construct($objParent) {
		
		$this->objParent = $objParent;
	}
	
	public function render($display, $strRender) {
			
		$inv = ($display === "invisible") ? "style='display:none'" : ""; 
		
		if ($this->boolHidden) $inv = "style='display:none'";
		if ($this->boolShow) $inv = "style='display:block'";
		
		if($this->boolAjaxLLCallable) $strRender = '<div ' . $inv . ' id="' . $this->strId . '_control">' . $strRender . '</div>';
		
		if ($display) 	echo $strRender;
		else 			return $strRender;
	}
	
	
	protected function setAttributes() {
		
		$strAttributes = "";
		
		$strAttributes .= !empty($this->strId) ? ' id="'. $this->strId .'"' : '';
		
		$strAttributes .= !empty($this->arrCssClasses) ? ' class="'. implode(" ", $this->arrCssClasses) .'"' : '';
		
		$strStyles = "";
		
		foreach($this->arrStyles as $style => $value) $strStyles .= $style . ":" . $value . "; "; 
	
		$strAttributes .= !empty($strStyles) ? ' style="'. $strStyles .'"' : '';
		
		foreach($this->arrCustomAttr as $attr => $value) $strAttributes .= ' ' . $attr . '="' . $value . '" ';
		
		return $strAttributes;
	}
	
	
	public function addStyle($styleName, $styleValue) {
		
		if (func_num_args() == 2) $this->arrStyles[$styleName] = $styleValue;
		else {
			
			$args = func_get_args();
			
			for ($i=0; $i < func_num_args(); $i=$i+2) $this->arrStyles[$args[$i]] = $args[$i+1];
		}
	} 
	

	public function setId($id) {
		
		//if(in_array($id, self::$arrId)) throw new MyException("ID_NOT_UNIQUE");
		
		if (in_array($id, self::$arrId)) $id = $this->changeId($id);
		
		self::$arrId[] = $id;
		$this->strId = $id;
	}

	protected function changeId($id) {

		$id = $id . "_1";
		if (in_array($id, self::$arrId)) return $this->changeId($id);
		else return $id;
	}
	
	
	public function setAjaxLLCallable($bool = true) {
	
		if (isset($this->strName) && $this->Id == "") $this->setId($this->strName);
		
		if($this->Id == "" && $bool) throw new MyException("NO_ID_FOR_AJAX_LLCallable");
		$this->boolAjaxLLCallable = $bool;
	}
	
	
	public function addCustomAttr($attr, $value) {
		
		$this->arrCustomAttr[$attr] = $value;
	}

	public function removeCustomAttr($attr) {
	
		foreach ($this->arrCustomAttr as $key => $val) {
			
			if ($key == $attr) unset($this->arrCustomAttr[$key]);	
		}
	}
	
	
	public function addCssClass($className) {

		$classes = explode(" ", $className);

		foreach ($classes as $class) {
			
			if(!in_array($class, $this->arrCssClasses)) $this->arrCssClasses[] = $class;
		}
	}
	
	
	public function removeCssClass($className) {
	
		foreach ($this->arrCssClasses as $key => $val) {
			
			if ($val == $className) unset($this->arrCssClasses[$key]);	
		}
	}
	
	

	public function addAjaxAction($ajaxAction) {

		$webpageParent = App::returnWebpageObject($this);
		$webpageParent->addAjaxAction($ajaxAction);
	}
	
	
	// this function echos the javascript of the late ajax call (ajax call within an ajax call element)
	// the identifier prevents the javascript to be added multiple times to the end of the file
	public function addAjaxActionLateBinding($ajaxAction, $identifier = "") { 

		echo '<div id="lateBinding">/*' . $identifier . '*/';
	
		ob_start();
		
		AjaxAction::bindLateAjaxAction($ajaxAction);
		
		$string = ob_get_contents();
		
		ob_end_clean();

		echo html_entity_decode($string);
			
		echo '</div>';
	}

	
	public function addServerAction($serverAction) {
		
		$webpageParent = App::returnWebpageObject($this);
		$webpageParent->addServerAction($serverAction);
	}
	
	
	
	public function __set($strName, $strValue) {
		
			switch ($strName) {
	
				case "Hidden": 
					$this->boolHidden = Type::check($strValue, 'boolean');
					break;
				case "Show": 
					$this->boolShow = Type::check($strValue, 'boolean');
					break;
				
				default: throw new MyException(array("NON_EXISTING_VAR", $strName));
			}
	}	
	
	public function __get($strName) {
		
		
			switch ($strName) {
					
					case 'Parent':
						return $this->objParent;
						break;
					case 'Id':
						return $this->strId;
						break;
					case 'Styles':
						return $this->arrStyles;
						break;
					case 'CssClasses':
						return $this->arrCssClasses;
						break;	
					case 'AjaxLLCallable':
						return $this->boolAjaxLLCallable;
						break;	
					case 'Hidden':
						return $this->boolHidden;
						break;
					case 'Show':
						return $this->boolShow;
						break;	
					case 'LLCallables':
						return $this->LLCallables;
						break;		
									

					default: throw new MyException("NON_EXISTING_VAR", $strName);
			}
	}
	
	
}

?>