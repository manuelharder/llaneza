<?php

class RadioButton extends FormControl {
		

	protected $strText;
	protected $boolChecked = false;
	protected $strSubmited;
	
	
	public function __construct($objParent, $strName) {
		
		parent::__construct($objParent, $strName, true);
				
		if ($this->objParent->Method == "post") $this->strSubmited = App::PostVar($strName);
		
		if ($this->objParent->Method == "get") $this->strSubmited = App::GetVar($strName);
		
		if ($this->strSubmited == $this->strValue && !empty($this->strValue)) $this->boolChecked = true;
	}
	
	
	public function render($display = true, $strRender = "") {		
			
		$checked = $this->boolChecked ? 'checked="checked"' : '';
		
		$value = ($this->strValue == "" && !is_numeric($this->strValue)) ? $this->strText : $this->strValue;
		
		$strRender = sprintf('<label><input type="radio" name="%s" value="%s" %s %s/> %s</label>',
					$this->strName,
					$value,
					$checked,
					$this->setAttributes(),
					$this->strText);
		
		return parent::render($display, $strRender);								
	}
	
	
	public function __set($strName, $value) {

		switch ($strName) {
	
			case "Checked": 
					$this->boolChecked = Type::check($value, "boolean");
					break;
			case "Text": 
					$this->strText = $value;
					break;
					
			default: parent::__set($strName, $value);
		}
	}
	
	
	public function __get($strName) {
		
			switch ($strName) {
					
					case 'Checked':
						return $this->boolChecked;	
					case 'Submited':
						return $this->strSubmited;
					case 'Text':
						return $this->strText;
						
						
					default:
						return parent::__get($strName);	
			}
	}
	
	
}

?>