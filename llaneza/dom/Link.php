<?php

class Link extends DOMControl {
	
	
	protected $strHref;
	protected $strLinkText;
	protected $strTarget;
	protected $arrGet = array();
	

	public function __construct($objParent, $href = "", $linkText = "", $target = "self") {
		
		parent::__construct($objParent);
		
		$this->strHref 		= $href;
		$this->strTarget 		= $target;
		$this->strLinkText 	= $linkText;
	}
	
	
	public function render($display = true, $strRender = "") {		
						
		$strRender = sprintf('<a href="%s" %s%s>%s</a>',
										$this->strHref, 
										$this->setAttributes(),
										($this->strTarget != "self") ? 'target="' . $this->strTarget . '"' : '',
										$this->strLinkText);

		return parent::render($display, $strRender);								
	}
	
	
	
	public function setGet($name, $value) {
		
		$this->arrGet[$name] = $value;
		
		if(strpos($this->strHref, "?"))  $this->strHref = $this->strHref . "&" . urlencode($name) . "=" . urlencode($value);
		else 										$this->strHref = $this->strHref . "?" . urlencode($name) . "=" . urlencode($value);
	}
	
	public function getGet($name) {
		
		if(isset($this->arrGet[$name])) return $this->arrGet[$name];
		else return false;
	}
	
	
	
	public function __set($strName, $value) {

		switch ($strName) {
	
			case "Href": 
					$this->strHref = $value;
					break;
			case "Target": 
					$this->strTarget = $value;
					break;
			default: parent::__set($strName, $value);	
		}
	}

	
	public function __get($strName) {
		
			switch ($strName) {
					
					case 'Href':
						return $this->arrCssClasses;
					case 'Target':
						return $this->strTarget;
					case 'Get':
						return $this->arrGet;	
				default:
						return parent::__get($strName);	
			}
	}
	
	
}

?>