<?php

class SelectBoxGroup extends FormControl {
		

	protected $strLabel;
	protected $sbItems = array();
	
	public function __construct($objParent) {
		
		parent::__construct($objParent, "EMPTY", true);				
	}
	
	
	public function render($display = true, $strRender = "") {		
			
				
		$strRender = sprintf('<optgroup label="%s" %s>%s</optgroup>',
					$this->strLabel,
					$this->setAttributes(),
					$this->renderItems());
		
		return parent::render($display, $strRender);								
	}
	
	
	public function setSelectBoxItem($item) {
		
		$this->sbItems[] = $item;
	}
	
	
	protected function renderItems() {

		if (empty($this->sbItems)) return false;
		
		$items = "";
		
		foreach ($this->sbItems as $item) {
			
			$items .= $item->render(false);
		}
		return $items;
	}
	
	
	
	public function __set($strName, $value) {

		switch ($strName) {
	
			case "Label": 
					$this->strLabel = $value;
					break;
					
			default: parent::__set($strName, $value);
		}
	}
	
	
	public function __get($strName) {
		
			switch ($strName) {
					
					case 'Label':
						return $this->strLabel;
						
						
					default:
						return parent::__get($strName);	
			}
	}
	
	
}

?>