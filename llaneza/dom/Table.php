<?php

class Table extends DOMControl {
		

	protected $data;
	protected $paging = 100;
	protected $start = 0;
	protected $variable;
	
	protected $arrTitle 		= array();
	protected $arrCellContent	= array();
	protected $arrStyling 		= array();
	protected $boolTitleRow 	= true;	
	protected $arrLength 		= "";

	protected $addIdAsClass 	= false;
	
	protected $cellPadding	= 0;
	protected $cellSpacing	= 0;
	protected $border			= 0;
	protected $width;
	
	protected $rowColorTitle 		= "";
	protected $rowColor 				= "";
	protected $rowColorAlternate 	= "";
	
	protected $counter = 0;
	
	public $LLCallables = array();	
		
	protected $boolPaging = true;
	protected $boolInAjaxedPanel;
	
	/**
	 * Creates a Table to display data
	 * 
	 * @param $objParent (Parent Object)
	 * @param $variable (the variable name the table is assign to, needed for ajax paging)
	 */
	public function __construct($objParent, $variable = false, $inAjaxedPanel = false) {

		parent::__construct($objParent);  
		
		$this->setId($variable);
		
		$this->boolInAjaxedPanel = Type::check($inAjaxedPanel, "boolean");
		
		$this->variable = $variable;		
	}
	
	
	public function bindData($data) {
		
		$this->data = $data;
		
		if(!is_array($data) && !is_object($data)) {
			
			$this->data = array();
		}
	}
	

	/**
	 * defines the columns to the output
	 * 
	 * @param $title (title of the column)
	 * @param $valueOrFunction - (-VALUE-> for a value or -FUNC-> for a function that will edit the columns content)
	 * @param $arrStyling - (array of styling for the column)
	 * @return void
	 */
	public function addColumn($title, $valueOrFunction, $arrStyling = null, $length = null) {
		
		$this->arrTitle[] 	= $title;
		$this->arrStyling[] = $arrStyling;		
		$this->arrLength[] 	= $length;
		
		$this->getCellContent($valueOrFunction);
	}
	
	/**
	 * sets the content of the column if it is a function,
	 * it will be executed in the parent class, where this class
	 * is called
	 * 
	 * @param $valueOrFunction - (-VALUE-> for a value or -FUNC->)
	 * @return void
	 */
	public function getCellContent($valueOrFunction) {
		
		//if (!$this->data) return false;
		
		if (strpos($valueOrFunction, "VALUE->")) 		$this->arrCellContent[] = substr($valueOrFunction, 8);
		
		elseif (strpos($valueOrFunction, "FUNC->")) 	{
			
			$functionName = substr($valueOrFunction, 7);
			
			if (!method_exists($this->Parent, $functionName)) throw new MyException('FUNC_ADDCOLUMN_ERROR');
			
			list($this->arrCellContent[], $this->data) = $this->Parent->$functionName($this->data);
		}
		
		else throw new MyException("SECOND_ADDCOLUMN_ERROR");
	}
	
	
	public function render($display = true, $strRender = "") {	

		$this->setRowColor();

		$interval = $this->getInterval();

		$strRender = "";
	
		if (!$this->data) return parent::render($display, $strRender);
	
		if ($this->boolPaging) $strRender .= $this->getPaginator();
		
		$strRender .= $this->returnTitleRow(); 
		
		foreach ($this->data as $key => $rowValues) {
			
			if($key < $interval[0] OR $key > $interval[1]) continue;
			
			$strRender .= $this->returnRows($rowValues);
		}

		$strRender = "<table " . $this->setTableAttr() . ">" . $strRender . "</table>"; 
		
		return parent::render($display, $strRender);								
	}
	
	
	protected function returnTitleRow() {
		
		if(!$this->boolTitleRow) return;
		
		$color = "";
		
		if(!empty($this->rowColorTitle)) $color = " bgcolor='" . $this->rowColorTitle . "'";

		
		$titleRow = "<tr" . $color . ">";
		
		foreach ($this->arrTitle as $title) $titleRow .= "<th>" . $title  . "</th>";
		
		$titleRow .= "</tr>";
		
		return $titleRow;	
	}
	
	
	protected function returnRows($rowValues) {
		
		$row = $this->getTrTag($rowValues);

		foreach ($this->arrCellContent as $key => $valueName) {
			
			$tdAttr = $this->getTdAttr($key);
			
			$row .= "<td " . $tdAttr . ">";
			
			if (!$this->arrLength[$key]) {
				
				if(is_array($rowValues)) 	$row .= $rowValues[$valueName];
			
				if(is_object($rowValues)) 	$row .= $rowValues->$valueName; 
			}
			else {

				$add = "";
				
				if(is_array($rowValues)) 	{
					
					if (strlen($rowValues[$valueName]) > $this->arrLength[$key]) $add = "...";
					$row .= substr($rowValues[$valueName], 0, $this->arrLength[$key]). $add;
				}
			
				if(is_object($rowValues)) 	{
					
					if (strlen($rowValues->$valueName) > $this->arrLength[$key]) $add = "...";
					$row .= substr($rowValues->$valueName, 0, $this->arrLength[$key]) . $add;
				}
			}
				
			$row .= "</td>";
		}
		
		$row .= "</tr>";
		
		return $row;	
	}
	
	
	protected function getTrTag($rowValues) {
		
		$this->counter += 1;

		$color = ($this->counter % 2) ? $this->rowColor : $this->rowColorAlternate;

		if($color != "") $color = " bgcolor='" . $color . "'";

		$id = "";

		if($this->addIdAsClass) $id = " class='" . $rowValues->id . "'";
		
		$tr = "<tr" . $color . $id . ">";
		
		return $tr;
	}
	
	
	protected function getTdAttr($key) {
		
		$style = "";
		
		if($this->arrStyling[$key]) 
			foreach ($this->arrStyling[$key] as $name => $value) $style .= $name . "='" . $value . "' ";
		
		return trim($style);
	}
	
	
	
	protected function getInterval() {
		 
		$end = $this->start + $this->paging - 1;
		
		if ($this->paging == "") $end = count($this->data);
		
		return array($this->start, $end);
	}
	
	
	protected function getPaginator() {
		
		$all 	= count($this->data);
		
		$total = "<span class='totalEntries'>Total: " . $all . "</span>";
		
		$prev = "prev"; 
		$next = "next";  
			
		$this->LLCallables[$this->variable] = new LLCallable($this); 
			
		$prevLink = $this->setUpPrevLink();
		$nextLink = $this->setUpNextLink();
		$pageNo   = $this->setUpPageNo($all);

		if ($pageNo == "<span>1</span>") return $total;
	
		if ($this->start > 0) 						$prev = $prevLink->render(false);	
		if ($this->start < ($all - $this->paging)) 	$next = $nextLink->render(false);
		
		$return  = $total . "<div class='navEntries'><span class='prevLink'>" . $prev . "</span>";
		$return .= "<span class='pageLink'>" . $pageNo . "</span>";
		$return .= "<span class='nextLink'>" . $next . "</span></div>";
		
		return $return;
	}
	
	
	public function setUpPrevLink() {
		
		$prevLink = new Link($this, "javascript:void(0)", "prev");
		$prevLink->setId("prev" . $this->variable);
		
		if (!$this->boolInAjaxedPanel)
			$this->addAjaxAction(new AjaxAction("click", $prevLink, "prev", $this->LLCallables, $this->objParent->{$this->variable}));
		else
			$this->addAjaxActionLateBinding(new AjaxAction("click", $prevLink, "prev", $this->LLCallables, $this->objParent->{$this->variable}));
		
		return $prevLink;
	}
	
	public function setUpNextLink() {
		
		$nextLink = new Link($this, "javascript:void(0)", "next");
		$nextLink->setId("next" . $this->variable);
			
		if (!$this->boolInAjaxedPanel)
			$this->addAjaxAction(new AjaxAction("click", $nextLink, "next", $this->LLCallables, $this->objParent->{$this->variable}));
		else
			$this->addAjaxActionLateBinding(new AjaxAction("click", $nextLink, "next", $this->LLCallables, $this->objParent->{$this->variable}));
		
		return $nextLink;
	}

	public function setUpPageNo($all) {
		
		$pages = "";
		
		for ($i = 0; $i < ($all/$this->paging); $i++) {
			
			$no = $i+1;
			
			if ($this->start/$this->paging == $i) $pages .= "<span>{$no}</span>";
			else {
			
				$pageLink = new PageLink($this, $i+1);
				$pages .= $pageLink->render(false);
			}
		}
		$this->addAjaxAction(new AjaxAction("click", "a.pageId", "gotoPage", $this->LLCallables, $this->objParent->{$this->variable}));		
		
		return $pages;
	}
	
	public function next() {
		$this->start += $this->paging; 
	}
	public function prev() {
		$this->start -= $this->paging; 
	}
	public function gotoPage() {
		$this->start = (App::PostVar('pageId')-1) * $this->paging; 
	}
	
	
	protected function setRowColor() {
		
		if(empty($this->rowColorAlternate)) $this->rowColorAlternate = $this->rowColor;
		if(empty($this->rowColorTitle))		$this->rowColorTitle		 = $this->rowColorAlternate;
	}
	
	
	protected function setTableAttr() {
		
		$attr  = "cellpadding='" . $this->cellPadding . "'";
		$attr .= " cellspacing='" . $this->cellSpacing . "'";
		$attr .= " border='" . $this->border . "'";
		
		if(!empty($this->width)) $attr .= "  width='" . $this->width . "'";
		
		$attr .= $this->setAttributes();
		
		return $attr;
	}
	
	public function noPaging() {
		
		$this->boolPaging = false;
	}
		
	public function __set($strName, $value) {
		
			switch ($strName) {

					case "Paging":
						$this->paging = Type::check($value,"NUMERIC");
						break;
					case "Pagination":
						$this->boolPaging = Type::check($value,"boolean");
						break;	
					case "CellPadding":
						$this->cellPadding = Type::check($value,"NUMERIC");
						break;	
					case "CellSpacing":
						$this->cellSpacing = Type::check($value,"NUMERIC");
						break;
					case "Border":
						$this->border = Type::check($value,"NUMERIC");
						break;
					case "Width":
						$this->width = $value;
						break;	
					case "RowColor":
						$this->rowColor = Type::check($value,"COLOR");
						break;
					case "AddIdAsClass":
						$this->addIdAsClass = Type::check($value,"boolean");
						break;		
					case "RowColorTitle":
						$this->rowColorTitle = Type::check($value,"COLOR");
						break;
					case "RowColorAlternate":
						$this->rowColorAlternate = Type::check($value,"COLOR");
						break;	
					case "TitleRow":
						$this->boolTitleRow = Type::check($value,"boolean");
						break;		
					case 'LLCallables':
						return $this->LLCallables;
						break;			
						
					default: parent::__set($strName, $value);
			}
	}
	
	
	public function __get($strName) {
		
			switch ($strName) {
					
					case 'Paging':
						return $this->paging;
					case 'Variable':
						return $this->variable;	
					case 'CellPadding':
						return $this->cellPadding;
					case 'CellSpacing':
						return $this->cellSpacing;
					case 'Border':
						return $this->border;
					case 'Width':
						return $this->width;					
					case 'RowColor':
						return $this->rowColor;
					case 'RowColorAlternate':
						return $this->rowColorAlternate;
					case 'RowColorTitle':
						return $this->rowColorTitle;
					case 'Data':
						return $this->data;			
					
					default: 
						return parent::__get($strName);
			}
	}
	
	
	
	
}

?>