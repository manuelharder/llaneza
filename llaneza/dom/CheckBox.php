<?php

class CheckBox extends FormControl {
		

	protected $strText;
	protected $boolChecked;
	protected $strSubmited;
	
	
	public function __construct($objParent, $strName) {
		
		parent::__construct($objParent, $strName, $this);
		
		if($this->objParent->Method == "post") $this->strSubmited = App::PostVar($strName);
		
		if($this->objParent->Method == "get") $this->strSubmited = App::GetVar($strName);
		
		if ($this->strSubmited != "") $this->boolChecked = true;
	}
	
	
	public function render($display = true, $strRender = "") {		
		
		$checked = $this->boolChecked ? 'checked="checked"' : '';
		
		if ($this->strValue == "") $this->strValue = $this->strText;
		
		$strRender = sprintf('<label><input type="checkbox" name="%s" value="%s" %s %s/> %s</label>',
					$this->strName,
					$this->strValue,
					$checked,
					$this->setAttributes(),
					$this->strText);
		
		return parent::render($display, $strRender);								
	}
	
	
	public function __set($strName, $value) {

		switch ($strName) {
	
			case "Checked": 
					$this->boolChecked = Type::check($value, "boolean");
					break;
			case "Text": 
					$this->strText = $value;
					break;
					
			default: parent::__set($strName, $value);
		}
	}
	
	
	public function __get($strName) {
		
			switch ($strName) {
					
					case 'Checked':
						return $this->boolChecked;	
					case 'Submited':
						return $this->strSubmited;
					case 'Text':
						return $this->strText;
						
						
					default:
						return parent::__get($strName);	
			}
	}
	
	
}

?>