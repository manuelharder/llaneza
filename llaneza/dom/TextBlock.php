<?php

class TextBlock extends DOMControl {
	
	
	protected $strText;
	protected $children = array();

	
	public function __construct($objParent, $id = "") {
		
		parent::__construct($objParent);
		
		if(!empty($id)) $this->setId($id);
	}
	
	
	public function setChildren($varName) {
		
		if (!in_array($varName, $this->children)) $this->children[] = $varName;
	}
	
	
	public function render($display = true, $strRender = "") {		
	
		$strRender = sprintf('<div %s >%s</div>',
									$this->setAttributes(),
									$this->strText);
				
		return parent::render($display, $strRender);
	}
	
		
	
	public function __set($strName, $value) {

		switch ($strName) {
	
			case "Text": 
				try {
					$this->strText = $value;
					break;
				} catch (Exception $e) {
					throw $e;
				}
				
			default: parent::__set($strName, $value);	
		}
	}

	
	public function __get($strName) {
		
			switch ($strName) {
					
					case 'Text':
						return $this->strText;
					case 'Children':
						return $this->children;
							
				default:
						return parent::__get($strName);	
			}
	}
	
	
}

?>