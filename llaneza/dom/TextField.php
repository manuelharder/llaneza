<?php

class TextField extends FormControl {
		

	protected $boolMultiline 	= false;
	protected $strType 			= "text";
	
	
	public function __construct($objParent, $strName) {
		
		parent::__construct($objParent, $strName);
				
		if ($this->objParent->Method == "post") $this->strValue = App::PostVar($strName);
		
		if ($this->objParent->Method == "get") $this->strValue = App::GetVar($strName);
	}
	
	
	public function render($display = true, $strRender = "") {		

		if(!$this->boolMultiline) {
			
			$strRender = sprintf('<input type="%s" name="%s" value="%s" %s />',
										$this->strType,
										$this->strName, 
										$this->strValue,
										$this->setAttributes());
		}
		else {
				$strRender = sprintf('<textarea name="%s" %s>%s</textarea>',
										$this->strName, 
										$this->setAttributes(),
										$this->strValue);
		}
		
		return parent::render($display, $strRender);								
	}
	
	
	public function __set($strName, $value) {

		switch ($strName) {
	
			case "Multiline": 
					$this->boolMultiline = Type::check($value, "boolean");
					break;
					
			case "Type": 
					$this->strType = Type::check($value, "INPUT");
					break;
		
			default: parent::__set($strName, $value);
		}
	}
	
	
	public function __get($strName) {
		
			switch ($strName) {
					
					case 'Multiline':
						return $this->boolMultiline;	
					
					case 'Type':
						return $this->strType;	
						
					default:
						return parent::__get($strName);	
			}
	}
	
	
}

?>