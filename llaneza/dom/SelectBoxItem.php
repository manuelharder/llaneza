<?php

class SelectBoxItem extends FormControl {
		

	protected $boolSelected = false;
	protected $strText;
	
	
	public function __construct($objParent, $group = false) {
		
		parent::__construct($objParent, "EMPTY", true);
				
		if (!$group) $this->objParent->setSelectBoxItem($this);
	}
	
	
	public function render($display = true, $strRender = "") {		
			
		$selected = $this->boolSelected ? 'selected="selected"' : '';
		
		$this->strValue = $this->strValue == "" ? $this->strText : $this->strValue;
		
		if ($this->objParent->Value != "") {
		
			$selected = ($this->objParent->Value == $this->strValue) ? 'selected="selected"' : '';	
		}
		
		$strRender = sprintf('<option value="%s" %s %s>%s</option>',
					$this->strValue,
					$selected,
					$this->setAttributes(),
					$this->strText);
		
		return parent::render($display, $strRender);								
	}
	
	
	public function __set($strName, $value) {

		switch ($strName) {
	
			case "Selected": 
					$this->boolSelected = Type::check($value, "boolean");
					break;
			case "Text": 
					$this->strText = $value;
					break;
					
			default: parent::__set($strName, $value);
		}
	}
	
	
	public function __get($strName) {
		
			switch ($strName) {
					
					case 'Selected':
						return $this->boolChecked;	
					case 'Text':
						return $this->strText;
						
						
					default:
						return parent::__get($strName);	
			}
	}
	
	
}

?>