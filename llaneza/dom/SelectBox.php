<?php

class SelectBox extends FormControl {
		

	protected $sbItems = array();
	protected $emptyItem = false;
	protected $size = 1;
	protected $multiple = false;
	protected $groups = array();
	
	
	public function __construct($objParent, $strName) {
		
		parent::__construct($objParent, $strName);	

		if ($this->objParent->Method == "post") $this->strValue = App::PostVar($strName);
		
		if ($this->objParent->Method == "get") $this->strValue = App::GetVar($strName);		
	}
	
	
	public function render($display = true, $strRender = "") {		

		$size = $this->size > 1 ? 'size="' . $this->size . '"' : "";
				
		$multi = $this->multiple ? 'multiple="multiple"' : "";
		
		$strRender = sprintf('<select name="%s" %s %s %s>%s</select>',
					$this->strName,
					$this->setAttributes(),
					$size,
					$multi,
					$this->renderItems());
		
		return parent::render($display, $strRender);								
	}
	
	// only called in the SelectBoxItem class!!!
	public function setSelectBoxItem($item) {
		
		$this->sbItems[] = $item;
	}
	
	public function setGroups($group) {
		
		$this->groups[] = $group;
	}
	
	
	protected function renderItems() {

		if (empty($this->sbItems)) throw new MyException("NO_SELECTBOX_ITEM");
		
		$items = $this->emptyItem ? "<option value=''></option>" : "";
		
		foreach ($this->sbItems as $item) {
			
			$items .= $item->render(false);
		}
		
		if (!empty($this->groups)) {
				
			foreach ($this->groups as $group) {
			
				$items .= $group->render(false);
			}
		}
		
		return $items;
	}
	
	
	public function __set($strName, $value) {

		switch ($strName) {
	
			case "EmptyItem": 
					$this->emptyItem = Type::check($value, "boolean");
					break;
			case "Size": 
					$this->size = Type::check($value, "NUMERIC");
					break;
			case "Multiple": 
					$this->multiple = Type::check($value, "boolean");
					break;
							
							
			default: parent::__set($strName, $value);
		}
	}
	
	
	public function __get($strName) {
		
			switch ($strName) {
					
					case 'EmptyItem':
						return $this->emptyItem;	
					case 'Size':
						return $this->size;
					case 'Multiple':
						return $this->multiple;
					case 'Items':
						return $this->sbItems;		
					default:
						return parent::__get($strName);	
			}
	}
	
	
}

?>