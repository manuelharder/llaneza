<?php

class Form extends DOMControl {
		

	protected $strMethod;
	protected $strAction;
	
	
	public function __construct($objParent, $strMethod = "post", $strAction = "") {

		parent::__construct($objParent);  
		$this->strAction = $strAction;
		$this->strMethod = $strMethod;
	}
	
	
	public function render($display = true, $strRender = "") {		
		
		if(empty($this->strAction)) {
			
			$path = explode("/", Router::getPath());
			$this->strAction =  $path[count($path)-1];
		}
				
		$strRender = sprintf('<form method="%s" action="%s" %s>',
										$this->strMethod, 
										$this->strAction,
										$this->setAttributes());	

		return parent::render($display, $strRender);																
	}
	
	
	public function end() {
		
		echo "</form>";
	}
	
	
	public function setMethod($strMethod) {
		
		$this->strMethod = Type::check($strMethod, "METHOD"); 
	}
	
	public function setAction($strAction) {
		
		$this->strMethod = $strAction; 
	}
	
	
	public function __set($strName, $strValue) {
		
			switch ($strName) {

					case "Action": 
					$this->strAction = $strValue;
					break;
					
					case "Method": 
					$this->strMethod = Type::check($strValue, "METHOD");
					break;
				
				default: throw new MyException("NON_EXISTING_VAR", $strName);
			}
	}
	
	
	public function __get($strName) {
		
			switch ($strName) {
					
					case 'Method':
						return $this->strMethod;
					case 'Action':
						return $this->strAction;

					default: 
						return parent::__get($strName);
			}
	}
	
	
}

?>