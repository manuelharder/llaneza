<?php

class AjaxAction {
		

	protected $strEvent;
	protected $strTrigger;
	protected $strFunction;
	protected $arrAjaxLLCallables 	= array();
	protected $arrObjLLCallables 	= array();
	protected $callStack 			= "";
	protected $arrJS				= array();
	protected $arrJSBefore			= array();
	
	protected $arrPostParams = array();
	
	protected static $sessionId = "";
	
	
	/**
	 * Creates new AjaxAction Object
	 * @param strEvent  - (click, mouseover or mouseout) 
	 * @param strTrigger - (ID or Class of the Element that triggers the Event) 
	 * @param strFunction - (Function name that will be executed after the ajax call) 
	 * @param arrLLCallables - (Array of the Objects that are going to be changed)
	 * @param panel - [optional] (Object of the Panel that makes the ajax call, if false the webpage class makes the call)
	 * @param arrPostParams - [optional] (Parameter that are posted with the ajax call)  
	 */
	public function __construct($strEvent, $objTrigger, $strFunction, $arrLLCallables = false, $panel = false, $arrPostParams = false, $allFormElements = true) {
		
		if (!$panel && is_object($objTrigger)) list($panel, $arrLLCallables) = App::returnAjaxVars($objTrigger);
				
		// not key exists for if the callables are in an array with 0,1,... for different ajax calls		
		elseif($panel && !array_key_exists(0, $panel->LLCallables)) $arrLLCallables = $panel->LLCallables;
						
		if ($panel) $this->callStack = App::CallStack($panel);
		
		$this->strEvent 		= $strEvent;

		$this->setAjaxTrigger($objTrigger);
				
		$this->strFunction 	= $strFunction;
		
		$arrPostParams["sessionId"] = self::returnSessionId();
		$arrPostParams["url"] = Router::getURL();		
		
		$this->arrPostParams = $arrPostParams;
		
		if(!$arrLLCallables) $this->arrAjaxLLCallables = false;
				
		else $this->extendAndSetLLCallables($allFormElements, $panel, $arrLLCallables);		
	}
	
	
	public function AddJSAfterUpdate($js) {
		
		$this->arrJS[] = $js;
	}
	public function AddJSBeforeUpdate($js) {
		
		$this->arrJSBefore[] = $js;
	}

	
	/**
	 * Bind trigger with ajax action
	 * @param objAjaxAction 
	 * Object with params Event (String), Trigger (Object that triggers the event), Function (String Functionname)
	 * LLCallables (Array that containes the ids and Objects that will be changed).
	 * @return prints the javascript.
	 */
	public static function bindAjaxAction($objAjaxAction) { 
		
		$strAjaxUpdate = self::returnJSforLLCallableUpdate($objAjaxAction);
		
		$strAjaxUpdate .= "if(data.indexOf('redirect>') > 0) { top.location.href = $(data).children('redirect').text(); } ";
				
		$strAjaxUpdate .= "$.addLateAjax($(data).children('#lateBinding').text()) ";
		
		$LLCallableString = ($objAjaxAction->AjaxLLCallables) ? implode("###", array_keys($objAjaxAction->AjaxLLCallables)) : "";
		
		$strParams = self::addCustomPostParameter($objAjaxAction);
		
		$jsAfterUpdate = implode(" ", $objAjaxAction->arrJS);
		$jsBeforeUpdate = implode(" ", $objAjaxAction->arrJSBefore);
		
		//$formnames = self::returnFormNames($objAjaxAction);
			
		//check for name of form element over LLCallables right now the name has to be the same as the variable name ---- not good at all
		
                $callablePosts = "";
                                
                //$objCalled = ($objAjaxAction->callStack != "") ? App::returnObjectFromCallStack($class, $objAjaxAction->callStack) : $class;
                
                /* is just needed if there is no form around the form fields, but it should always have a form!!
                foreach (array_keys($objAjaxAction->AjaxLLCallables) as $callable) {
                   
                    if (get_class($objCalled->$callable) == "SelectBox" or get_class($objCalled->$callable) == "TextField") {
                        
                        $callablePosts .= "posts += '&" . $objCalled->$callable->Name . "=' + $('#" . $objCalled->$callable->Id . "').val();";
                    }
                    
                     if (get_class($objCalled->$callable) == "RadioButton") {
                        
                        $callablePosts .= "if ($('#" . $objCalled->$callable->Id . "').is(':checked')) posts += '&" . $objCalled->$callable->Name . "=' + $('#" . $objCalled->$callable->Id . "').val();";
                    }
                }
		*/

        // if there is no trigger, no action needed
        if (!$objAjaxAction->Trigger) return false;
                
		printf("$(document).on('%s', '%s', function() { 

						%s
						
						var get = '&' + $(this).attr('postAjax'); 
						
						// the values of the user input fields will be posted and in AjaxBackend added to the value of the objects
						var posts = ''; 
                                                %s   

				 		$(this).parents('form').find('input').not(':checkbox, :radio').each(function(){
							 posts += '&' + $(this).attr('name') + '=' + $(this).val();	
						});
						$(this).parents('form').find('textarea, select').each(function(){
							posts += '&' + $(this).attr('name') + '=' + $(this).val();	
						});
						$(this).parents('form').find(':checkbox').each(function(){
							 	if($(this).is(':checked')) posts += '&' + $(this).attr('name') + '=' + $(this).val();
							 	else posts += '&' + $(this).attr('name') + '=';
						});
						$(this).parents('form').find(':radio').each(function(){  
							 	if($(this).is(':checked')) posts += '&' + $(this).attr('name') + '=' + $(this).val();	
						});
 
						$.ajax({ 
							type: 'POST',
			  				url: '/assets/backend/ajax.php',
		      			data: 'funcCalled=%s&LLCallables=%s&callStack=%s' + posts + '%s' + get,
		      			contentType: 'application/x-www-form-urlencoded;charset=utf-8',
		      			async: false, 
		      			success: function(data) { 
		      				//if(data.indexOf('</b> on line <b>') > 0) window.location = window.location.href;
			  					%s; %s;
			  				},
						});	
					});", 
				$objAjaxAction->Event,
				$objAjaxAction->Trigger,
				$jsBeforeUpdate,
                $callablePosts,
				$objAjaxAction->FunctionName,
				$LLCallableString,
				$objAjaxAction->callStack,
				$strParams,
				$strAjaxUpdate,
				$jsAfterUpdate 
				);
		
	} 
	
	
	/**
	 * Bind trigger with ajax action
	 * @param objAjaxAction 
	 * Object with params Event (String), Trigger (Object that triggers the event), Function (String Functionname)
	 * LLCallables (Array that containes the ids and Objects that will be changed).
	 * @return prints the javascript.
	 */
	public static function bindLateAjaxAction($objAjaxAction) { 
		
		$strAjaxUpdate = self::returnJSforLLCallableUpdate($objAjaxAction);
		
		$strAjaxUpdate .= "if($(data).children('redirect').length > 0) top.location.href = $(data).children('redirect').text();";
			
		$LLCallableString = ($objAjaxAction->AjaxLLCallables) ? implode("###", array_keys($objAjaxAction->AjaxLLCallables)) : "";
		
		$strParams = self::addCustomPostParameter($objAjaxAction);
		
		$jsAfterUpdate = implode(" ", $objAjaxAction->arrJS);
		$jsBeforeUpdate = implode(" ", $objAjaxAction->arrJSBefore);

		
		printf("$('%s').unbind();
					$(document).on('%s', '%s', function() { 

						%s
						
						var get = '&' + $(this).attr('postAjax'); 
						
						// the values of the user input fields will be posted and in AjaxBackend added to the value of the objects
						var posts = ''; 
				 		$(this).parents('form').find('input').not(':checkbox, :radio').each(function(){
							 posts += '&' + $(this).attr('name') + '=' + $(this).val();	
						});
						$(this).parents('form').find('textarea, select').each(function(){
							posts += '&' + $(this).attr('name') + '=' + $(this).val();	
						});
						$(this).parents('form').find(':checkbox').each(function(){
							 	if($(this).is(':checked')) posts += '&' + $(this).attr('name') + '=' + $(this).val();
							 	else posts += '&' + $(this).attr('name') + '=';
						});
						$(this).parents('form').find(':radio').each(function(){ 
							 	if($(this).is(':checked')) posts += '&' + $(this).attr('name') + '=' + $(this).val();	
						});

						$.ajax({ 
							type: 'POST',
			  				url: '/assets/backend/ajax.php',
		      			data: 'funcCalled=%s&LLCallables=%s&callStack=%s' + posts + '%s' + get,
		      			contentType: 'application/x-www-form-urlencoded;charset=utf-8',
		      			async: false, 
		      			success: function(data) { 
		      				if($(data).attr('class') != 'result') window.location = window.location.href;
			  					%s; %s; 
			  				},
						});	
					});", 
				$objAjaxAction->Trigger,
				$objAjaxAction->Event,
				$objAjaxAction->Trigger,
				$jsBeforeUpdate,
				$objAjaxAction->FunctionName,
				$LLCallableString,
				$objAjaxAction->callStack,
				$strParams,
				$strAjaxUpdate,
				$jsAfterUpdate  
				);
		
	} 
	
	
	
	/**
	 * Bind trigger with ajax action
	 * @param objTrigger (normally a object that triggers the ajax call, but it can also be a class for several element)
	 * needs the Id if it is one trigger or the class if there are several to identify the trigger
	 */
	protected function setAjaxTrigger($objTrigger) {
		
		if (is_object($objTrigger)) { 
			
			if (is_a($objTrigger, "Button")) if ($objTrigger->Id == "") $objTrigger->setId($objTrigger->Name);
			
			if($objTrigger->Id == "") throw new MyException("NO_ID_FOR_AJAX_TRIGGER");
			
			$this->strTrigger 	= "#" . $objTrigger->Id;
		}
		else $this->strTrigger 	= $objTrigger;
	}
	
	
	protected function extendAndSetLLCallables($allFormElements, $panel, $arrLLCallables) {
		
		$formLLCallables = array();
		
		if ($allFormElements && $panel) {
			
			$formLLCallables = App::getFormLLCallables(get_object_vars($panel)); 
			$arrLLCallables  = array_merge($arrLLCallables, $formLLCallables); 
		}
		
		foreach ($arrLLCallables as $name => &$LLCallable) {

			if (!is_array($LLCallable->LLCallable)) { 
			
				$LLCallable->LLCallable->setAjaxLLCallable();
				$this->arrAjaxLLCallables[$name] = array($LLCallable->LLCallable->Id, $LLCallable->Effect); 
			}
			else {
				
				$ids = array();
				
				foreach ($LLCallable->LLCallable as $LLCallableObj) {
				
					$ids[] = $LLCallableObj->Id;
					$effect = $LLCallable->Effect;
					$LLCallableObj->setAjaxLLCallable();
				}
				$this->arrAjaxLLCallables[$name] = array($ids, $effect);
			}	
		} 
		$this->arrObjLLCallables = $arrLLCallables;		
	}
	
	
	protected static function returnJSforLLCallableUpdate($objAjaxAction) {
		
		$strAjaxUpdate = "";
		
		if(is_array($objAjaxAction->AjaxLLCallables)) {
			
			foreach ($objAjaxAction->AjaxLLCallables as $ajaxLLCallable) {
		
				if (is_array($ajaxLLCallable[0])) {
					
					foreach ($ajaxLLCallable[0] as $id) $strAjaxUpdate .= self::setAjaxUpdateString($id, $ajaxLLCallable[1]);
					
				}
				
				$strAjaxUpdate .= self::setAjaxUpdateString($ajaxLLCallable[0], $ajaxLLCallable[1]);
			}
		}
		
		return $strAjaxUpdate;
	}
	
	
	protected static function setAjaxUpdateString($id, $effect) {
		
		$strAjaxUpdate = "$('#" . $id ."_control').html($(data).children('#" . $id ."_control').html());";
				
		if ($effect) {
				
			$strAjaxUpdate .= "$('#" . $id ."_control').css('display','none');";
			$strAjaxUpdate .= "$('#" . $id ."_control')." . $effect .";";
		}
		else $strAjaxUpdate .= "$('#" . $id ."_control').css('display','block');";
		
		return $strAjaxUpdate;
	}
		
	
	protected static function addCustomPostParameter($objAjaxAction) {

		$strParams = "";

		if (is_array($objAjaxAction->PostParams)) {
			
			foreach ($objAjaxAction->PostParams as $name => $value) $strParams .= $name . "=" . $value . "&";
		}
		
		return ($strParams != "") ? "&" . $strParams : "";
	}
	
	
	protected static function returnFormNames($objAjaxAction) {
		
		$formnames = array();
		
		foreach ($objAjaxAction->arrObjLLCallables as $LLCallable) {
			
			if (!is_array($LLCallable->LLCallable)) { 
			
				if (is_a($LLCallable->LLCallable, "FormControl")) $formnames[] = $LLCallable->LLCallable->Name;
			}	
			else {
			
				foreach ($LLCallable->LLCallable as $LLCallableObj) {
	
					if (is_a($LLCallableObj, "FormControl")) $formnames[] = $LLCallableObj->Name;
				}
			}
		}
		return $formnames = implode("###", $formnames);
	}
	
	
	public static function returnSessionId() {
		
		$sessionId = SessionID::getID();
		
		if (isset($_POST['sessionId'])) return $_POST['sessionId'];
		
		return $sessionId;
	}
	
	public function __set($strName, $value) {
		
			switch ($strName) {

				
			}
	}
	
	
	public function __get($strName) {
		
			switch ($strName) {
					
					case 'Event':
						return $this->strEvent;
					case 'Trigger':
						return $this->strTrigger;
					case 'FunctionName':
						return $this->strFunction;
					case 'AjaxLLCallables':
						return $this->arrAjaxLLCallables;
					case 'callStack':
						return $this->callStack;
					case 'PostParams':
						return $this->arrPostParams;	
			}
	}
	
	
	
	
}

?>