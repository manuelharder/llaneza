<?php

class ServerAction {
		

	protected $strEvent;
	protected $objTrigger;
	protected $strFunction;
	protected $strCallStack = "";
	
	
	/**
	 * Creates new ServerAction Object
	 * is mainly used for clicks on elements that aren't buttons (example: text, div,...)
	 * IT IS USED WHEN THERE IS NO FORM - otherwise check if button send via GET or POST
	 * @param strEvent  - (click, mouseover or mouseout) 
	 * @param objTriger - (Object that triggers the Event) 
	 * @param strFunction - (Function name that will be executed after the event) 
	 * @param panel - [optional] (Object of the Panel that makes the Server call, if false the webpage class makes the call)  
	 */
	public function __construct($strEvent, $objTrigger, $strFunction, $panel) {

		if($panel) $this->strCallStack = App::CallStack($panel);
		
		$this->strEvent 		= $strEvent;
		
		$this->objTrigger 	= $objTrigger;
		
		if($this->objTrigger->Id == "") throw new MyException("NO_ID_FOR_Server_TRIGGER");
		
		$this->strFunction 	= $strFunction;

	}

	/**
	 * Bind trigger with server action
	 * bind the information and post creates a pseudo form to send it with
	 * @param objServerAction 
	 * @return prints the javascript.
	 */
	public static function bindServerAction($objServerAction) { 
				
		printf("$(document).ready(function() {
						$('#%s').bind('%s', function() { 
		
						var posts = ''; 
				 		$('input').each(function(){
							posts = posts + '&' + $(this).attr('name') + '=' + $(this).val();	
						});
						var arrData = [];
						arrData['function'] = '%s';
						arrData['callStack'] = '%s';
						post(arrData,'/%s');
					});
				});", 
				$objServerAction->Trigger->Id,
				$objServerAction->Event,
				$objServerAction->FunctionName,
				$objServerAction->CallStack,
				Router::getPath()
		);
		
	} 
	
		
	public function __set($strName, $value) {
		
			switch ($strName) {
							
			}
	}
	
	
	public function __get($strName) {
		
			switch ($strName) {
					
					case 'Event':
						return $this->strEvent;
					case 'Trigger':
						return $this->objTrigger;
					case 'FunctionName':
						return $this->strFunction;
					case 'CallStack':
						return $this->strCallStack;
			}
	}
	
	
	
	
}

?>