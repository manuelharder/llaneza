<?php

class AjaxClickAction extends AjaxAction {
		
	
	public function __construct($objTrigger, $strFunction, $arrLLCallables = false, $panel = false, $arrPostParams = false, $allFormElements = true) {

		parent::__construct("click", $objTrigger, $strFunction, $arrLLCallables, $panel, $arrPostParams, $allFormElements);
	}
	
}

?>