<?php

class AjaxBackend {
		

	protected $strFunction;
	protected $arrAjaxLLCallables = array();
	protected $callStack = "";
	protected $arrValues = array();
	
	
	/**
	 * Handle Ajax Call
	 */
	
	public function __construct($post) {

		$this->strFunction = App::PostVar('funcCalled');
                
		if($_POST['LLCallables'] != "") $this->arrAjaxLLCallables = explode("###", $_POST['LLCallables']);
		else $this->arrAjaxLLCallables = false;
		
		$this->callStack = $_POST['callStack'];
		
		// get the posted values and prepare them to be added to the values of the variables they belong to
		foreach ($_POST as $name => $val) $this->arrValues[$name] = App::PostVar($name);	
	}
	
	
	/**
	 * Handle Ajax Call
	 * 
	 * returnObjectFromClassStack returns callstack, where the ajax call took place
	 * setAjaxPost adds all the ajax posted values to the objects
	 * $objCalled->{$this->strFunction}();	calls the function that does changes, saves or ... the objects
	 * the last loops render the elements that have been changed by the ajax call
	 */
	public function handleCall($class) {
	
		// check if ajax call triggers function in 'panel' or 'webpage' class
		$objCalled = ($this->callStack != "") ? App::returnObjectFromCallStack($class, $this->callStack) : $class;

		//if the elements that will be changed are user input field change to new values
		if($this->arrAjaxLLCallables)  $this->setAjaxPost($objCalled);
				
		$objCalled->{$this->strFunction}();			
			
		$return = "";
	
		// render the elements that the ajax call changes
		if($this->arrAjaxLLCallables) {
			
			foreach ($this->arrAjaxLLCallables as $LLCallable) {
				
				if(!isset($objCalled->$LLCallable)){
					// for the tables, need to find better solution!!!!!
					$return .= $objCalled->Parent->$LLCallable->render();
					continue;
				} 
			
				if (!is_array($objCalled->$LLCallable)) {

					if (!is_a($objCalled->$LLCallable, "TextBlock")) {
						
						$return .= $objCalled->$LLCallable->render();
						continue;
					}
					
					// a TextBlock can contain of children panel, they will be rendered as well
					if (isset($objCalled->$LLCallable->Children[0])) {
						
						$objCalled->$LLCallable->Text = ""; 
						
						foreach ($objCalled->$LLCallable->Children as $child) {
							
							if (!is_array($objCalled->$child)) {
							
								$objCalled->$LLCallable->Text .= $objCalled->$child->render(false);
								continue;
							}	
							
							foreach ($objCalled->$child as $key => $childItem) {
			
								$objCalled->$LLCallable->Text .= $childItem->render(false);
							}
						}
					}
					$return .= $objCalled->$LLCallable->render();
				}
				
				
				else {

					foreach ($objCalled->$LLCallable as $LLCallable) 
						$return .= $LLCallable->render();
				}		
			}
		}
		return $return;
	} 
	

	protected function setAjaxPost($objCalled) { 
	
		foreach ($this->arrAjaxLLCallables as $LLCallable) {
		
			if (!isset($objCalled->$LLCallable)) continue;
			
			if (!is_array($objCalled->$LLCallable)) {
				
				if (is_a($objCalled->$LLCallable, "FormControl")) $objCalled->$LLCallable->setAjaxPost($this->arrValues);
				
				if (!is_a($objCalled->$LLCallable, "TextBlock")) continue;
				
				if (!isset($objCalled->$LLCallable->Children[0])) continue;

				// if it is a TextBlock it can contain of children, the childrens post values will be set
				foreach ($objCalled->$LLCallable->Children as $child) {
					
					// it there is only one child panel
					if (!is_array($objCalled->$child)) {
					
						// set all the form elements post values
						foreach (get_object_vars($objCalled->$child) as $name => $value) {
							
							if (is_a($objCalled->$child->$name, "FormControl")) $objCalled->$child->$name->setAjaxPost($this->arrValues);
						}
						continue;
					}	
					
					// if there are than one child panel loop them
					foreach ($objCalled->$child as $key => $childItem) {
						
						foreach (get_object_vars($childItem) as $name => $value) {
							
							if (is_a($childItem->$name, "FormControl")) $childItem->$name->setAjaxPost($this->arrValues);
						}
					}
				} // ends the setting of the children panel posts
			}
					
			else {
				
				foreach ($objCalled->$LLCallable as $LLCallableObj) 
					if(is_a($LLCallableObj, "FormControl")) $LLCallableObj->setAjaxPost($this->arrValues);		
			}
		}
	}		
	
}
