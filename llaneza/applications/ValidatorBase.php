<?php
	class ValidatorBase {

		
	/**
	 * Check if the user input is an email
	 * @param email - (email that has to be checked)
	 * @return Boolean of the result 
	 */	
		public static function validEmail($email) {
   
			$isValid = true;
   		$atIndex = strrpos($email, "@");
   		
   		if (is_bool($atIndex) && !$atIndex) $isValid = false;
   		else {
		      
   			$domain = substr($email, $atIndex+1);
		      $local = substr($email, 0, $atIndex);
		      $localLen = strlen($local);
		      $domainLen = strlen($domain);
      		
		      if ($localLen < 1 || $localLen > 64) $isValid = false;
     
      		else if ($domainLen < 1 || $domainLen > 255) $isValid = false;
      
      		else if ($local[0] == '.' || $local[$localLen-1] == '.') $isValid = false;
      
      		else if (preg_match('/\\.\\./', $local)) $isValid = false; //local part has 2 consecutive dots
      
      		else if (!preg_match('/^[A-Za-z0-9\\-\\.]+$/', $domain)) $isValid = false;
      
      		else if (preg_match('/\\.\\./', $domain)) $isValid = false; // domain part has two consecutive dots
            
      		else if (!preg_match('/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/', str_replace("\\\\","",$local))) {
		         
      			// character not valid in local part unless 
		         // local part is quoted
		         if (!preg_match('/^"(\\\\"|[^"])+"$/', str_replace("\\\\","",$local))) $isValid = false;   
		      }
   		}
   	return $isValid;
		}
		
		
		
	public static function checkEmail(&$email, &$error, $message) {
		
		if (!self::validEmail($email->Value)) {
                            
			$email->addCssClass("error");
			$error[] = $message;
		} 
		else $email->removeCssClass("error");
	}	
	
		
	/**
	 * Check if the user input is empty
	 * and add or remove the error css class that signals an error 
	 * @param txtField - (text field Object that has to be checked)
	 * @param error - (error Array that contains the error messages)
	 * @param message - (String message that will be added to the error Array when the text field is empty)
	 * @return void 
	 */	
		public static function emptyField(&$txtField, &$error, $message) {
			
			if ($txtField->Value == "") {
                           
				$txtField->addCssClass("error");
				$error[] = $message;
			} 
			else $txtField->removeCssClass("error");
		}
		
	
	
		
	/**
	 * Check if the user input is numeric
	 * and add or remove the error css class that signals an error 
	 * @param txtField - (text field Object that has to be checked)
	 * @param error - (error Array that contains the error messages)
	 * @param message - (String message that will be added to the error Array when the text field is not numeric)
	 * @return void 
	 */	
		public static function isNumericField(&$txtField, &$error, $message) {
			
			if (!is_numeric($txtField->Value)) {
                           
				$txtField->AddCssClass("error");
				$error[] = $message;
			} 
			else $txtField->RemoveCssClass("error");
		}
		
		
	/**
	 * Compare to Passwords
	 * and add or remove the error css class that signals an error 
	 * @param txtField1 - (text field Object that has to be checked)
	 * @param txField2 - (text field Object that has to be checked)
	 * @param error - (error Array that contains the error messages)
	 * @return error message or empty String
	 */	
		public static function checkPasswordsEqual(&$txtField1, &$txtField2, &$error) {
			
			if ($txtField1->Value == "" or $txtField2->Value == "") {
                            
				$txtField1->AddCssClass("error");
				$txtField2->AddCssClass("error");
				$error[] = "Please enter a password and confirm it.";
				return;
			} 
			elseif ($txtField1->Value != $txtField2->Value) {
                            
				$txtField1->AddCssClass("error");
				$txtField2->AddCssClass("error");
				$error[] = "The password and the confirmed password differ.";
			} 
			else {
				
				$txtField1->RemoveCssClass("error");
				$txtField2->RemoveCssClass("error");
			}
		}


	}
