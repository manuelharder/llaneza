<?php

class WebpageBase {
	
	protected $strDescription	= "";
	protected $strKeywords 		= "";
	protected $strRobots 		= "index, follow";
	protected $strTitle			= "Llaneza";
	
	protected $strUrl;
	protected $objStructure;
	
	protected $arrAjaxActions		= array();
	protected $arrServerActions	= array();
	
	protected $strTemplateName = null;
	protected $strHeader = '_header.php';
	protected $strFooter = '_footer.php';

	protected static $boolLoggedIn = false;
	protected $private = false;
		
	public $LLCallables = array();
	protected $jsFiles = array();
	public $form;	
		
	
	/**
	 * Renders the Webpage  - is called in index.php
	 * 
	 * @return the tpl of the webpage and renders the webpage class   
	 */
	public static function Run() { 
		
		if(isset($_SESSION['logName']) && isset($_SESSION['logPassword'])) 
			self::$boolLoggedIn = LogIn::CheckLogIn($_SESSION['logName'], $_SESSION['logPassword'], true);

		$strWebpage = get_called_class();
		
		$class = new $strWebpage();
		
		$class->strUrl 		= Router::getURL();
		$class->objStructure = Router::getStructure();
	
		if (self::$boolLoggedIn or !$class->private) $class->CreateWebpage();
				
		try {
			
			include_once(__TPL__ . $class->strHeader);
			
			if (!self::$boolLoggedIn && $class->private) echo "Private page!"; //you can create a panel for this
	
			else { 
			
				if (!$class->strTemplateName) $class->strTemplateName = App::getTplName();
			
				include_once(__TPL__ . $class->strTemplateName . '.tpl.php');
			}
			
			include_once(__TPL__ . $class->strFooter);
						
      } 
      catch (Exception $e) {
     		throw $e;
      }
	}
	

	public function setPrivate($bool) {
		
		$this->private = $bool;
	}
	
	
	
	public function addAjaxAction($objAjaxAction) {
		
		$this->arrAjaxActions[] = $objAjaxAction;
	}
	
	// this function echos the javascript of the late ajax call (ajax call within an ajax call element)
	// the identifier prevents the javascript to be added multiple times to the end of the file
	public function addAjaxActionLateBinding($ajaxAction, $identifier = "") { 

		echo '<div id="lateBinding">/*' . $identifier . '*/';
	
		ob_start();
		
		AjaxAction::bindLateAjaxAction($ajaxAction);
		
		$string = ob_get_contents();
		
		ob_end_clean();

		echo html_entity_decode($string);
			
		echo '</div>';
	}
	
	
	public function addServerAction($objServerAction) {
		
		$this->arrServerActions[] = $objServerAction;

		$functionName = $objServerAction->FunctionName;

		if(isset($_POST['function'])) $objServerAction->Trigger->Parent->$functionName();
	}

	public function setJsFile($file, $external = false) {

		$this->jsFiles[] = array("file" => $file, "external" => $external);
	}

	
	
	public function __set($strName, $strValue) {
		
			switch ($strName) {
				
				default: throw new MyException("NON_EXISTING_VAR", $strName);	
			}
	}
	
	public function __get($strName) {
		
			switch ($strName) {
					
					case 'Title':
						return $this->strTitle;
						break;
					case 'Description':
						return $this->strDescription;
						break;
					case 'Keywords':
						return $this->strKeywords;
						break;
					case 'Robots':
						return $this->strRobots;
						break;
					case 'Url':
						return $this->strUrl;
						break;
					case 'AjaxActions':
						return $this->arrAjaxActions;			
						break;
					case 'ServerActions':
						return $this->arrServerActions;			
						break;
					case 'strTemplateName':
						return $this->strTemplateName;			
						break;
					case 'strHeader':
						return $this->strHeader;			
						break;
					case 'strFooter':
						return $this->strFooter;			
						break;
					case 'LoggedIn':
						return self::$boolLoggedIn;			
						break;	
					case 'LLCallables':
						return $this->LLCallables;			
						break;
					case 'Structure':
						return $this->objStructure;			
						break;
					case 'jsFiles':
						return $this->jsFiles;			
						break;					
						
					default: throw new MyException("NON_EXISTING_VAR", $strName);					
		}
	}
	
	
}
