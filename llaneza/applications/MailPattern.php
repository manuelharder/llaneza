<?php

include_once(__LL_EXT__ . 'Zend/Mail.php');

abstract class MailPattern extends AppPattern {
		
	protected $from 			= "";	
	protected $from_name 	= "";
	protected $email;

	
	public function __construct($objParent, $email = false, $from_name = false) {
		
		$this->objParent = $objParent;
			
		$this->email = new Zend_mail('utf-8'); 
		
		if ($email) 		$this->from 		= $email;
		if ($from_name) 	$this->from_name 	= $from_name;
		
		$this->email->setFrom($this->from, $this->from_name);
		$this->email->setReplyTo($this->from);
	}
	
	
	
}
