<?php

class Log {
	
	/**
	 * Write into log file
	 * @return void 
	 */	
	public static function writeLogin($name) { 
	
		$file = __FILES__ ."log/login.txt";
		
		if(!is_file($file)) return false;
		
		$ip 			= $_SERVER["SERVER_ADDR"];
		$browser 	= $_SERVER["HTTP_USER_AGENT"];
		$date			= date("Y-m-d H:i");
		
		$string		= "Datestamp: " . $date . " User: " . $name . " IP: " . $ip . " Browser: " . $browser . "\r\n";

		$handle = fopen($file, "a");
		 
  		fwrite($handle, $string);
  		fclose($handle);		
	}
	
}
?>