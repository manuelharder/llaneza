<?php


class TypeBase {
	
	
	/**
	 * Get Object to call a function
	 * @param value - (value that has to be checked)
	 * @param type - (type that has to be checked)
	 * @return error if the type is wrong or the value 
	 */	
	public static function check($value, $type) {
	
		$getType = gettype($value);
		
		if($type == "boolean") {
			
			if($getType != $type) throw new MyException("TYPE_ERROR_BOOL");
			else return $value;
		}
		
		if($type == "METHOD") {
			
			if($value != "post" && $value != "get") throw new MyException("TYPE_ERROR_METHOD");
			else return $value;
		}
		
		if($type == "INPUT") {
			
			if($value != "password" && $value != "text" && $value != "hidden"  && $value != "file") 
				throw new MyException("TYPE_ERROR_INPUT");
			else return $value;
		}
		
		if($type == "ARRAY") {
			
			if(!is_array($value)) throw new MyException("TYPE_NO_ARRAY");
			else return $value;
		}
	
		if($type == "NUMERIC") {
			
			if(!is_numeric($value)) throw new MyException("TYPE_NOT_NUMERIC");
			else return $value;
		}
		
		if($type == "COLOR") {
			
			if(!preg_match('/^#[a-fA-F0-9]{3,6}$/i', $value)) throw new MyException("NOT_A_COLOR");
			else return $value;
		}
		
		return $value;
	}
	
	
	
	public static function convertInBool($value) {
		
		if ($value) return true;
		else 			return false;
	}
	
}
