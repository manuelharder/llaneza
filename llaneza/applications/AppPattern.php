<?php

abstract class AppPattern {
	
	
	protected $objParent;
	
	protected $db;
	
	
	/**
	 * this class is the parent class of every custom APP
	 * @param objParent - (parent object)
	 */
	public function __construct($objParent) {
		
		$this->objParent = $objParent;
		
		$this->db = new DBObserver();
	}
	
	
	public function __get($strName) {
		
		
			switch ($strName) {
					
					case 'Parent':
						return $this->objParent;
						break;
					case 'DB':
						return $this->db;
						break;

					default: throw new MyException("NON_EXISTING_VAR", $strName);
			}
	}
	
	
}
