<?php


class RouterBase {

	protected static $path;
	protected static $folder;
	protected static $structure;
	
	public static function route() {
		
		self::setPathAndFolder();
		
		$db  = MyDB::getDB();
		
		$page = $db->queryObjRow("SELECT * FROM ll_page WHERE urlpath = '" . self::$path . "' AND folder = '" . self::$folder . "'");
		
		self::$structure = $page;
	
		
		if (!empty($page)) {
			
			$folder = ($page->folder) ? $page->folder . "/" : "";
			 
			if (file_exists($folder . $page->link)) {
				
				include_once $folder . $page->link;
				return true;
			}
		}
		
		
		else {
			self::$structure = new DefaultRouter();			
			self::$structure->link = (self::$path == "/") ? "home.php" : self::$path . ".php";
			self::$structure->urlpath = self::$path;
			self::$structure->folder = self::$folder;
			self::$structure->headline = "";	

			if (self::$path == "/") self::$path = "home";	
		
			if (!empty(self::$folder)) $folder = self::$folder . "/";
			else $folder = ""; 
		
			
			if (file_exists($folder . self::$path . ".php")) { 
				include_once $folder . self::$path . ".php";
				return true;
			}
			
			elseif (file_exists($folder . self::$path . "/index.php")) { 
				include_once $folder . self::$path . "/index.php";
				return true;
			}	
			
			elseif (file_exists($folder . "index.php") && $folder != "") {
				include_once $folder . "index.php";
				return true;
			}				
							
		}
		
        self::$structure = new DefaultRouter();
		self::$structure->link = "four0four.php";
		self::$structure->urlpath = "four0four";
		self::$structure->folder = "";
		self::$structure->headline = "";	
		
		include_once 'four0four.php';
	}
	
	public static function getPath() {
		
		return self::$path;
	}
	
	
	public static function getURL() {
		
		$folder = !empty(self::$structure->folder) ?  self::$structure->folder . "/" : "";
		
		return __PATH__ . $folder . self::getPath();
	}
	
	
	public static function classToCall() {

		if (is_array(self::$structure)) return App::returnClass(self::$structure["link"]);
		
		return str_replace("-", "", App::returnClass(self::$structure->link));
	}
	
	
	public static function getStructure() {
		
		return self::$structure;
	} 
	
	
	public static function setPathAndFolder() {
		
		$path = App::GetVar("r");
		
		//remove last / if exists
		if(substr($path, -1, 1) == "/") $path = substr($path, 0, strlen($path)-1);
		
		if (empty($path)) $path = "/";
		
		$p = explode("/", $path);
		

		// create camelCase
		$helper = explode("-", $p[count($p)-1]);
		foreach ($helper as &$value) {
			$value = ucfirst($value);
		}
		self::$path = lcfirst(implode("", $helper));
		
		unset($p[count($p)-1]);
		
		self::$folder = implode("/", $p);
		
		if (self::$path == "") self::$path = "/";
	}
	
	
}


