<?php

class LLCallable {

	protected $LLCallable;
	protected $displayEffect;
	
	/**
	 * Class for the elements that will be changed after the ajax call
	 * @param object - (is the object of the dom classes which will be rerendered after the ajax call)
	 * @param displayEffect (javascript effect for displaying the rendered dom)   
	 */
	public function __construct($object, $displayEffect = false) {
		
		$this->LLCallable 		= $object;
		$this->displayEffect = $displayEffect;
	}
	
	
	public function __get($strName) {
		
			switch ($strName) {
					
					case 'Effect':
						return $this->displayEffect;
					case 'LLCallable':
						return $this->LLCallable;
			}
	}
	
}
