<?php

//function set_exception_handler can make it pretty!!

class MyExceptionBase extends Exception {
	
	
	/**
	 * Exeption handling
	 * @param type - (the type of the exception)
	 * @return triggers the exception 
	 */	
	public function __construct($type) {
		
		if (is_array($type)) {
			$name = $type[1];
			$type = $type[0];
		}

		switch ($type) {
			
			case "ID_NOT_UNIQUE":
				$this->message = "THE ID OF AN ELEMENT HAS TO BE UNIQUE!";
				break;
			
			case "NAME_NOT_UNIQUE":
				$this->message = "THE NAME OF THE INPUT FIELD HAS TO BE UNIQUE!";
				break;
				
			case "NO_FORM_NAME":
				$this->message = "THE FORM ELEMENT HAS TO HAVE A NAME!";
				break;
				
			case "TYPE_ERROR_BOOL":
				$this->message = "THE TYPE OF THE VARIABLE HAS TO BE A BOOLEAN!";
				break;
			case "TYPE_ERROR_METHOD":
				$this->message = "THE METHOD OF THE FORM HAS TO BE 'get' OR 'post'!";
				break;
			case "TYPE_ERROR_INPUT":
				$this->message = "THE TYPE OF THE INPUT FIELD HAS TO BE 'password', 'text' OR 'hidden'!";
				break;
			case "TYPE_NO_ARRAY":
				$this->message = "THE TYPE OF THE DATA TO OUTPUT IN A TABLE HAS TO BE AN ARRAY!";
				break;	
			case "TYPE_NOT_A_COLOR":
				$this->message = "THE TYPE OF THE VARIABLE HAS TO BE A COLOR!";
				break;
			case "TYPE_NOT_NUMERIC":
				$this->message = "THE TYPE OF THE VARIABLE HAS TO BE A NUMBER!";
				break;		
							
			case "NO_BINDING_POSSIBLE":
				$this->message = "THE TYPE OF THE DATA WITHIN THE ARRAY YOU BIND TO THE TABLE HAS TO BE AN OBJECT OR AN ARRAY!";
				break;	
				
			case "NON_EXISTING_VAR":
				$this->message = "THE VARIABLE DOESN'T EXIST! {$name}";
				break;
				
			case "NO_ID_FOR_AJAX_TRIGGER":
				$this->message = "THE TRIGGER OF THE AJAX ACTION HAS TO HAVE AN ID!";
				break;
				
			case "NO_ID_FOR_AJAX_LLCallable":
				$this->message = "THE ELEMENT THAT AJAX TRIGGERS HAS TO HAVE AN ID!";
				break;	
				
			case "FUNC_ADDCOLUMN_ERROR":
				$this->message = "THE FUNCTION THAT SHOULD EDIT THE DATA FOR THE DATAGRID DOES NOT EXIST!";
				break;	
			case "SECOND_ADDCOLUMN_ERROR":
				$this->message = "THE SECOND VALUE OF THE ADDCOLUMN FUNCTION HAS TO START WITH '-VALUE->' OR '-FUNC->'!";
				break;

			case "NO_SELECTBOX_ITEM":
				$this->message = "THE SELECTBOX HAS NO ITEM!";
				break;

			case "NOT_A_COLOR":
				$this->message = "THE VALUE IS NOT A COLOR, IT HAS TO BE '#ff3333' OR '#2222AA' FOR EAMPLE!";
				break;
		}
						
		
	}
	
	public function __toString() {
		
		$return = '"' . $this->message . '"' . "\r\n\r\n";
		$return .= $this->getTraceAsString();
		print_r("<pre>");
		return $return;
	}

}

?>