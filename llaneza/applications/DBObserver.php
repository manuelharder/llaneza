<?php

class DBObserver {
	
	/**
	 * For ajax calls
	 * it needs to re-assign the db object
	 */	
	
	public function queryObjArray($sql) { 

		$db = MyDB::getDB();
		return $db->queryObjArray($sql);
	}
	
	
	public function queryObjRow($sql) { 
	
		$db = MyDB::getDB();
		return $db->queryObjRow($sql);
	}
	
	
	public function querySingleItem($sql) {
		 
		$db = MyDB::getDB();
		return $db->querySingleItem($sql);
	}

	
	public function execute($sql) {
	
		$db = MyDB::getDB();
		return $db->execute($sql);
	}
	
}

?>