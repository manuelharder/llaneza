<?php



class AppBase {

	public static $arrJS = array();
	
	/**
	 * To ensure that the POST variables can't harm the application
	 * @param name - (value of the POST variable)
	 * @return escaped, save value of the POST variable   
	 */
	public static function PostVar($name) { 
	
		if(!isset($_POST[$name])) return "";
				
		if(is_array($_POST[$name])) {
			
			foreach($_POST[$name] as $key => $value) {
					
				if (!get_magic_quotes_gpc()) $value = addslashes($value);
			
				$arr[$key] = htmlspecialchars($value, ENT_NOQUOTES);
			}
			return $arr;	
		}

		$result = (!get_magic_quotes_gpc()) ? addslashes($_POST[$name]) : $_POST[$name];
			
		return trim(htmlspecialchars($result, ENT_NOQUOTES));	
	}
	
	
	/**
	 * To ensure that the GET variables can't harm the application
	 * @param name - (value of the GET variable)
	 * @return escaped, save value of the GET variable   
	 */
	public static function GetVar($name) { // MAGIC-QUOTES-EINF�GEN!!!!!!!!
	
		if(!isset($_GET[$name])) return "";
		
		$result = $_GET[$name];
			
		if (!get_magic_quotes_gpc()) $result = addslashes($result);
			
		$result = htmlspecialchars($result, ENT_NOQUOTES);
		
		return trim($result);
	}


	/**
	 * To ensure that the SESSION variables can't harm the application
	 * @param name - (value of the SESSION variable)
	 * @return escaped, save value of the SESSION variable   
	 */
	public static function Session($name) { 
	
		if(!isset($_SESSION[$name])) return "";
		
		if(is_array($_SESSION[$name])) {
			
			foreach($_SESSION[$name] as $key => $value) {
					
				if (!get_magic_quotes_gpc()) $value = addslashes($value);
			
				$arr[$key] = htmlspecialchars($value, ENT_NOQUOTES);
			}
			return $arr;	
		}

		$result = (!get_magic_quotes_gpc()) ? addslashes($_SESSION[$name]) : $_SESSION[$name];
			
		return trim(htmlspecialchars($result, ENT_NOQUOTES));	  
	}
	
	
	
	public static function format($text)  {
		
		$result = htmlspecialchars($text);
		$result = nl2br($result);
		return $result;    
	}
	
	
	/**
	 * Redirect
	 * @param get - (String message which will be added as GET variable)
	 * @param file - (String where to go to)
	 * @param ajax - (Boolean if it is a ajax call)
	 * @return redirect or when ajax call return redirect tag with path   
	 */
	public static function redirect($get, $file, $ajax = false) {

		$get = urlencode($get);
		
		if($ajax) {
			
			$get = strpos($file, "?") ? '&msg=' . $get : '?msg=' . $get;
			
			echo "<redirect>" . $file . $get . "</redirect>";
			return; 
		}
		
		if(empty($get)) header('Location: ' . $file);
		
		else {
			
			if(strpos($file, "?")) 	header('Location: ' . $file . '&msg=' . $get);
			else 							header('Location: ' . $file . '?msg=' . $get);
		}
	}


	public static function runJS($func) {
			
			echo "<function>" . $func . "</function>";		
	}
	
	
	/**
	 * Intervall for pagination
	 * @param from - (Int ot the first element to be shown)
	 * @param to - (Int of the last element to be shown)
	 * @param total - (Int of the total entries)
	 * @return 2 Ints - (start and end values)  
	 */
	public static function getIntervall($from, $to, $total) {
	
		if(empty($from) && is_numeric($from)) { 
			
			$start 		= 0; 
			$intervall 	= $to; 
		} else { 
			
			$start 		= $from; 
			$intervall 	= $from + $to; 
		}
		if($total < $intervall) $intervall = $total;
		
		return array($start, $intervall);
	}
	
	/**
	 * Save JavaScript
	 * @param strJs - (String of JavaScript that will be added to the Webpage in the footer tpl)
	 * @return void   
	 */	
	public static function executeJS($strJS) {
		
		self::$arrJS[] = $strJS;
	}
	
	/**
	 * Add JavaScript to webpage
	 * @return JavaScript   
	 */	
	public static function getJS() {
		
		if(empty(self::$arrJS)) return "";
		
		$return = '<script type="text/javascript">$(document).ready(function() { ';
		
		foreach(self::$arrJS as $js) $return .= $js;
		 
		$return .= ' });</script>';
		
		return $return;
	}
	
	
	/**
	 * Get CallStack -> get layer/ object model
	 * @param panel - (current panel)
	 * @param strCallStack - (for recursive calls)
	 * @return String of object model (without the webpage class example "GlobalCMS->PanelHeader")  
	 */	
	public static function CallStack($panel, $strCallStack = array()) {
		
		if (is_a($panel, "Panel") or is_a($panel, "Webpage")) $strCallStack[] =  get_class($panel);
		else {
			
			$strCallStack[] =  $panel->Variable;
			//$panel  	= $panel->Parent;
			$cmsAjax = true;
		}
		
		if(!is_a($panel, "Webpage")) return self::CallStack($panel->Parent, $strCallStack);
		
		else  {
			
			if (!isset($cmsAjax)) array_pop($strCallStack);
			krsort($strCallStack);
			return implode("->", $strCallStack);
		}
	}
	
	public static function returnAjaxVars($objTrigger) {
		
		if (is_a($objTrigger->Parent, "Webpage")) return array($objTrigger->Parent, $objTrigger->Parent->LLCallables);
		else return self::returnAjaxVars($objTrigger->Parent);
	}
	
	/**
	 * Get Object to call a function
	 * @param baseObject - (Object of Webpage class)
	 * @param callStack - (String of the callStack)
	 * @return Object to execute a function 
	 */	
	public static function returnObjectFromCallStack($baseObject, $callStack) {
		
		$arrCallStack = explode("->", $callStack);
		
		$object = $baseObject;
		
		foreach($arrCallStack as $objects) $object = $object->$objects;
		
		return $object;
	}

	
	/**
	 * Get Object Webpage Object
	 * @param object - (Child Object)
	 * @return Webpage Object - (last Parent)
	 */	
	public static function returnWebpageObject($object) {
				
		if(!is_a($object, "Webpage")) return self::returnWebpageObject($object->Parent);
		
		else return $object;
	}
	
	
	
	public static function returnClass($link) {
		
		if($link == "home") return $link;
		
		return ucfirst(substr($link, 0, strpos($link, ".")));
	}
	
	
	public static function getTplName() {
				
		$structure = Router::getStructure();
		
		$folder = ($structure->folder) ? $structure->folder . "/" : "";
		
		if (!strpos($structure->link, ".")) return $folder . $structure->link;
		
		return $folder . substr($structure->link, 0, strpos($structure->link, "."));
	}
	
	
	public static function getFormLLCallables($panel) {
		
		$LLCallables = array();
		
		foreach($panel as $name => $classVars) {
			
			if (is_object($classVars))
				if (is_a($classVars, "FormControl")) $LLCallables[$name] = new LLCallable($classVars); 
		}
		return $LLCallables;
	}

	public static function addJsFiles($arrFiles) {

		if (empty($arrFiles)) return;

		foreach ($arrFiles as $fileName) {
			
			if ($fileName["external"])
				echo '<script type="text/javascript" src="' . $fileName["file"] . '"></script>';
			else
				echo '<script type="text/javascript" src="' . JS . $fileName["file"] . '"></script>';
		}
	} 
	
	
	public static function ve($var) {
	
		print_r("<pre>");
		print_r($var);
		exit();
	}	
	
	public static function v($var) {
	
		print_r("<pre>");
		print_r($var);
	}
}

?>