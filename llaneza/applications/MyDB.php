<?php


class MyDB {

	protected $mysqli;
	protected $database;
	
	protected static $dbObj = null;
	
	protected function __construct() {  
		
		require_once(__CONFIG__ . '#con_data.php');
	
		$this->database = $database;
		
		$this->mysqli = new mysqli($host, $user, $password);
		
		mysqli_select_db($this->mysqli, $database);
		
		$this->mysqli->query("SET CHARACTER SET 'utf8'"); 
		
		if(mysqli_connect_errno()) {
		
			printf("<p>Sorry, no connection! %s\n</p>", mysqli_connect_error());
			$this->mysqli = FALSE; 
			exit();
		}
	}
	
	
	public static function getDB() {
		
		if(self::$dbObj == null) self::$dbObj = new self;
		
		return self::$dbObj;
		
	}

	
	public static function closeDBConnection() {
		
		if(self::$dbObj != null) self::$dbObj->close();
	}
	

	//public function __destruct() { $this->close(); }

	
	protected function close() {
	
		if($this->mysqli) {
			
			$this->mysqli->close(); 
			$this->mysqli = false;
		}
	}

	
	public function queryObjArray($sql, $database = "") { 
		
		if(!empty($database)) 	mysqli_select_db($this->mysqli, $database);
		else							mysqli_select_db($this->mysqli, $this->database);
		
		if($result = $this->mysqli->query($sql)) {
			if($result->num_rows) {
			
				while($row = $result->fetch_object()) $result_array[] = $row;
				 
				return $result_array;
			} 
			else return false;
		} 
		else { 
			
			print $sql;
			printf("<p>Error: %s</p>", $this->mysqli->error); return false;
		}
	}
	
	

	public function queryObjRow($sql, $database = "") {  // function for sending querys to the database
	
		if(!empty($database)) 	mysqli_select_db($this->mysqli, $database);
		else							mysqli_select_db($this->mysqli, $this->database);
		
		if($result = $this->mysqli->query($sql)) {
			
			if($result->num_rows) return $result->fetch_object();
				
			else return FALSE;
		} 
		else { 
			print $sql;
			printf("<p>Error: %s</p>", $this->mysqli->error); 
			return FALSE;
		}
	}
	
	
	
	public function querySingleItem($sql, $database = "") {  // function for sending querys to the database
		
		if(!empty($database)) 	mysqli_select_db($this->mysqli, $database);
		else							mysqli_select_db($this->mysqli, $this->database);
	
		if($result = $this->mysqli->query($sql)) {
		
			if($row = $result->fetch_array()) return $row[0];
			else return false;
		} 
		else { 
			
			print $sql;
			printf("<p>Error: %s</p>", $this->mysqli->error); return false;
		}
	}
	

	public function execute($sql, $database = "") {
	
		if(!empty($database)) 	mysqli_select_db($this->mysqli, $database);
		else							mysqli_select_db($this->mysqli, $this->database);	
		
		if($this->mysqli->query($sql)) {
			
			if(str_replace("INSERT", "INSERT", $sql)) return $this->mysqli->insert_id;
			return true;
		} 
		else {
			
			print $this->mysqli->error;
			print $sql;
			return false;
		}	
	}


	public function runSQL($sql) {

    	if(!empty($database)) 	mysqli_select_db($this->mysqli, $database);
		else					mysqli_select_db($this->mysqli, $this->database);	

    	$this->runClean($sql); 
    }



    public function runClean($sql) {

    	if (!$this->mysqli->multi_query($sql)) {
    		$this->mysqli->error;
		}
		do {
	    	if ($res = $this->mysqli->store_result()) $this->res->free();
		} while ($this->mysqli->more_results() && $this->mysqli->next_result());
    }


}

