<?php

class SessionID {
	
    protected static $id = null;
        
    public static function getID() {
		
        if(self::$id == null) self::$id = rand(1, 9999999);

        return self::$id;

    }
        
}
?>