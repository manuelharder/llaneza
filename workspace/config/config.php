<?php
session_start();
define ('__DROOT__',  $_SERVER['DOCUMENT_ROOT'] . "/");

if(strpos($_SERVER['DOCUMENT_ROOT'], "xampp")) {
	
	define ('__SUB__',  			'');	
	
	define ('LL', 		'http://www.ll.local/');
	define ('ROOT', 	'http://www.workspace.local/');

	define ('__LL__',					__DROOT__ . __SUB__ . '../llaneza/');
	define ('__LL_ACTIONS__',		__DROOT__ . __SUB__ . '../llaneza/actions/');
	define ('__LL_APPS__',  		__DROOT__ . __SUB__ . '../llaneza/applications/');
	define ('__LL_DOM__',  			__DROOT__ . __SUB__ . '../llaneza/dom/');
	define ('__LL_EXT__',  			__DROOT__ . __SUB__ . '../llaneza/extensions/');
	define ('__LL_EXT__ZEND__',  	__DROOT__ . __SUB__ . '../llaneza/extensions/Zend/');

}
else {
	
	define ('__SUB__',  			'');	
	
	define ('LL', 		'http://www.framework.holidaywebs.de/');
	define ('ROOT', 	'http://www.holidaywebs.de/');
	
	define ('__LL__',					__DROOT__ . __SUB__ . '../framework/');
	define ('__LL_ACTIONS__',		__DROOT__ . __SUB__ . '../framework/actions/');
	define ('__LL_APPS__',  		__DROOT__ . __SUB__ . '../framework/applications/');
	define ('__LL_DOM__',  			__DROOT__ . __SUB__ . '../framework/dom/');
	define ('__LL_EXT__',  			__DROOT__ . __SUB__ . '../framework/extensions/');
	define ('__LL_EXT__ZEND__',  	__DROOT__ . __SUB__ . '../framework/extensions/Zend/');
}


define('__PATH__', ROOT . __SUB__);

define ('LL_ASSETS',  			LL . 'assets/');

define ('__CONFIG__',	__DROOT__ . __SUB__ . 'config/');
define ('__TPL__',  		__DROOT__ . __SUB__ . 'templates/');
define ('__APP__',  		__DROOT__ . __SUB__ . 'applications/');
define ('__EXT__',  		__DROOT__ . __SUB__ . 'extensions/');

define ('__FILES__',  	__DROOT__ . __SUB__ . '_files/');

define ('CSS',  		ROOT . __SUB__ . "assets/" . 'css/');
define ('IMAGES',  	ROOT . __SUB__ . "assets/" . 'images/');
define ('JS',  		ROOT . __SUB__ . "assets/" . 'js/');

define ('CSS_EXAMPLES',  		ROOT . __SUB__ . "assets/" . 'examples/css/');
define ('IMAGES_EXAMPLES',  	ROOT . __SUB__ . "assets/" . 'examples/images/');
define ('JS_EXAMPLES',  		ROOT . __SUB__ . "assets/" . 'examples/js/');


// functionality needs to be added
//define ('LOG_AJAX',  		true);
//define ('DEBUGGING_AJAX', 	true);


function __autoload($className) {

	$className = $className . ".php";

	if(file_exists(__LL_APPS__ . $className)) 					include_once __LL_APPS__ . $className;
	else if(file_exists(__LL__ . $className)) 					include_once __LL__ . $className;
	else if(file_exists(__LL_ACTIONS__ . $className)) 			include_once __LL_ACTIONS__ . $className;
	else if(file_exists(__LL_DOM__ . $className)) 				include_once __LL_DOM__ . $className;
	else if(file_exists(__LL_EXT__ . $className)) 				include_once __LL_EXT__ . $className;
	else if(file_exists(__LL_EXT__ZEND__ . $className)) 		include_once __LL_EXT__ZEND__ . $className;
	
	else if(file_exists(__APP__ . $className))					include_once __APP__ . $className;
	else if(file_exists(__APP__ . 'default/' . $className))	include_once __APP__ . 'default/' . $className;
	else if(file_exists(__APP__ . 'buttons/' . $className))	include_once __APP__ . 'buttons/' . $className;
	else if(file_exists(__EXT__ .  $className))					include_once __EXT__ . $className;
	
	else if(file_exists(__DROOT__ . __SUB__ . 'panel/' . $className))					include_once __DROOT__ . __SUB__ . 'panel/' . $className;
			
	else if(file_exists(__DROOT__ . __SUB__ . lcfirst($className))) 					include_once __DROOT__ . __SUB__ . lcfirst($className);
	else if(file_exists(__DROOT__ . __SUB__ . "example/" . lcfirst($className))) include_once __DROOT__ . __SUB__ . "example/" . lcfirst($className);
	
	else echo __LL_EXT__ . $className;
}


	
?>