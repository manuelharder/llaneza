<?php

class Ajaxedpanel extends Webpage {


	public $ExamplePanel;
	public $link;
	public $loadArea;
	
	public function CreateWebpage() {

		$this->strHeader = "_header_examples.php";
		
		$this->link = new Link($this, "javascript:void(0)", "load panel");
		$this->link->setId("link");
		
		$this->loadArea = new TextBlock($this, "loadArea");
		
		$this->LLCallables["loadArea"] = new LLCallable($this->loadArea);
		
		$this->addAjaxAction(new AjaxAction("click", $this->link, "loadExample", $this->LLCallables));
	}
	
	
	public function loadExample() {
		
		$this->ExamplePanel = new ExamplePanel($this);
		
		$this->loadArea->Text = $this->ExamplePanel->render(false); 
	}
	
		
}


?>