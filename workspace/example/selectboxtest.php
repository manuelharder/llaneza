<?php

class SelectboxTest extends Webpage {

	
	public $sbExample;
	
	public function CreateWebpage() {

		$this->strHeader = "_header_examples.php";
		
		$this->form	= new Form($this);
		
		$this->sbExample = new SelectBox($this->form, "sel1");
		$this->sbExample->EmptyItem = true;
		
		$item 			= new SelectBoxItem($this->sbExample);
		$item->Text 	= "item1";
		$item->Value 	= "value1";
		$item->Selected = true;
		
		$item 			= new SelectBoxItem($this->sbExample);
		$item->Text 	= "item2";
		$item->Value 	= "value2";
			
	}
	
}




?>