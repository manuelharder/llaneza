<?php

class Ajaxform extends Webpage {

	
	public $txtName;
	public $txtText;
	
	public $cbBeer;
	public $cbWine;	
	
	public $radioDavid;
	public $radioMichael;
	
	public $btnSubmit;
	
	public $msg;
	
	public function CreateWebpage() {

		$this->strTitle = "Example of an ajaxed form"; 

		// name and direction of the template file, here: templates/example/form.tpl.php
		$this->strTemplateName = "example/form1";
		$this->strHeader = "_header_examples.php";
		
		// the form instance needs to be the parent of all it's form elements
		$this->form = new Form($this);
		
		$this->txtName = new TextField($this->form, "name");
	
		$this->txtText = new TextField($this->form, "text");
		$this->txtText->Multiline = true;
		
		$this->cbBeer = new CheckBox($this->form, "beer");
		$this->cbBeer->Text = "beer";
		$this->cbWine = new CheckBox($this->form, "wine");
		$this->cbWine->Text = "wine";
		
		
		$this->radioDavid = new RadioButton($this->form, "singer");
		$this->radioDavid->setId("david");
		$this->radioDavid->Text = "David Hasselhoff";
		
		$this->radioMichael = new RadioButton($this->form, "singer");
		$this->radioMichael->setId("michael");
		$this->radioMichael->Text = "Michael Jackson";
		
		
		$this->btnSubmit = new Button($this->form, "submit", true);
		$this->btnSubmit->Value = "Submit";
		
		// TextBlock needs an ID for the Ajax call
		$this->msg = new TextBlock($this);
		$this->msg->setId("msg");

		$this->LLCallables["msg"] 			= new LLCallable($this->msg, "fadeIn(500)");
		
		// "click" event trigger by the "btnSubmit", Ajax calls the function "submit"
		$this->addAjaxAction(new AjaxAction("click", $this->btnSubmit, "submit", $this->LLCallables));
	}
	
	
	public function submit() {
	
		$error = "";

		Validator::emptyField($this->txtName, $error, "Please enter your name.");
				
		if (!empty($error)) {

			$this->msg->Text = "";
			$this->msg->Text = implode("<br/>", $error);
			$this->msg->removeCssClass("success");
			$this->msg->addCssClass("error");
		}
		
		else {
			
			$beverage = array();
			if ($this->cbBeer->Checked) $beverage[] = $this->cbBeer->Value;
			if ($this->cbWine->Checked) $beverage[] = $this->cbWine->Value; 
			
			$this->msg->Text = "Hi " . $this->txtName->Value . "! This form has been submited via Ajax.<br/>";
			
			$this->msg->Text .= "Text:<br/>" . App::format($this->txtText->Value);
			
			if (!empty($beverage)) $this->msg->Text .= "<br/>Beverage: " . implode(", ", $beverage) . "<br/>";
			
			$this->msg->Text .= "Singer: " . $this->radioDavid->Value;
			
			$this->msg->addCssClass("success");
		}
		
	}
	
}




?>