<?php

class Atable extends Webpage {

	public $table;
	public $pnlMessage;
	public $txtName;
	public $btnSubmit;
	
	public function CreateWebpage() {

		$this->strTitle = "Example for the Table class"; 
		$this->strHeader = "_header_examples.php";
		
		$this->form	= new Form($this);
		$this->form->addCssClass("saveForm");
				
		$this->txtName = new TextField($this->form, "txtName");
		
		$this->btnSubmit = new Button($this->form, "submitNew", true);
		$this->btnSubmit->setValue("save");
		$this->btnSubmit->setId("submit");
		$this->btnSubmit->addCssClass("submitBtn");
		
		$this->table = new Table($this, "table");
		
		$db  = MyDB::getDB();
		 
		$page = $db->queryObjArray("SELECT * FROM example_db");
				
		$this->table->bindData($page);
		
		$this->table->addColumn("id", "-VALUE->id", array("align" => "center", "width" => "50"));
		$this->table->addColumn("name", "-VALUE->name");
		$this->table->addColumn("action", "-FUNC->actions", array("align" => "center", "width" => "50"));
		
		$this->table->Paging = 4;
		$this->table->addCssClass("table");
		$this->table->RowColor 				= "#e9e2cc";
		$this->table->RowColorAlternate 	= "#F2ECD9";
				
		$this->pnlMessage = new TextBlock($this);		
		$this->pnlMessage->setId("msg");
		
		if (App::GetVar('msg')) {
			
			$this->pnlMessage->addCssClass("success");
			$this->pnlMessage->Text = App::GetVar('msg');
		}
		
		$this->LLCallables["pnlMessage"] = new LLCallable($this->pnlMessage, "fadeIn(500)");
				
		$this->addAjaxAction(new AjaxAction("click", "a.delete", "deleteEntry", $this->LLCallables));
		$this->addAjaxAction(new AjaxAction("click", $this->btnSubmit, "save", $this->LLCallables));
	}
	
	
	public function actions($data) {

		foreach($data as &$value) {
			
			$delButton 		 = new DeleteLink($this, $value->id);
			$value->actions = $delButton->render(false);
		}
		return array("actions", $data);
	}
	
	
	public function save() {
		
		$error = array();

		Validator::emptyField($this->txtName, $error, "Please enter a name.");
				
		if (!empty($error)) {

			$this->pnlMessage->Text = implode("<br/>", $error);
			$this->pnlMessage->removeCssClass("success");
			$this->pnlMessage->addCssClass("error");
		}
		
		else {
			$this->pnlMessage->removeCssClass("error");
			$this->pnlMessage->addCssClass("success");
			
			$db = MyDB::getDB();
			$db->execute("INSERT INTO example_db SET name ='" . $this->txtName->Value . "'");
			App::redirect("Entry has been saved.", $_SERVER['HTTP_REFERER'], true);
		}
	}
	
	
	public function deleteEntry() {

		$db = MyDB::getDB();
		$db->execute("DELETE FROM example_db WHERE id = " . App::PostVar("deleteId"));
		App::redirect("Entry has been deleted.", $_SERVER['HTTP_REFERER'], true);
	}
	
}


?>