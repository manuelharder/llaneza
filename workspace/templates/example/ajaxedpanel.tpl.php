

<div class="container">
	
	<div class="sidebar">

		<?php echo ExampleMenu::returnMenu(); ?>

	</div>

	<div id="content">
		
		<h1><?php echo $class->strTitle; ?></h1>

		<div class="wrapper">
			
			<?php $class->loadArea->render(); ?>
			
			<?php $class->link->render(); ?>
			
		</div>
	</div>
</div>