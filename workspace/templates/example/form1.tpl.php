

<div class="container">
	
	<div class="sidebar">

		<?php echo ExampleMenu::returnMenu(); ?>
		
	</div>

	<div id="content">
		
		<h1><?php echo $class->strTitle; ?></h1>

		<div class="wrapper">
<?php

			$class->msg->render("invisible");

			$class->form->render();
?>
			
			<div class="formRow even">
				<label class="formLabel">Name:</label><?php $class->txtName->render(); ?>
			</div>
			<div class="formRow odd">
				<label class="formLabel">Some text:</label><?php $class->txtText->render(); ?> 
			</div>
			<div class="formRow even">
				<label class="formLabel">Your beverage:</label><?php $class->cbBeer->render(); ?><?php $class->cbWine->render(); ?> 
			</div>
			<div class="formRow odd">
				<label class="formLabel">Favourite Singer:</label>
				<?php $class->radioMichael->render(); ?><?php $class->radioDavid->render(); ?> 
			</div>
			<div class="formRow even">
				<label class="formLabel"></label><?php $class->btnSubmit->render(); ?> 
			</div>
			
			<?php $class->form->end(); ?>

		</div>
	</div>
</div>