

<div class="container">
	
	<div class="sidebar">

		<?php echo ExampleMenu::returnMenu(); ?>

	</div>

	<div id="content">
		
		<h1>Example of a SelectBox</h1>

		<div class="wrapper">
		
			<?php $class->form->render(); ?>
		
			<?php $class->sbExample->render(); ?>
			
			<?php $class->form->end(); ?>	
			
		</div>
	</div>
</div>