

<div class="container">
	
	<div class="sidebar">

		<?php echo ExampleMenu::returnMenu(); ?>

	</div>

	<div id="content">
		
		<h1><?php echo $class->strTitle; ?></h1>

		<div class="wrapper">
		
			<?php $class->pnlMessage->render(); ?>
		
			<br/>
			
			<?php $class->form->render(); ?>
			<div class="formRow odd">
				<p class="formLabel">Name of beverage:</p><?php $class->txtName->render(); ?>
			</div>
			<div class="formRow even">
				<?php $class->btnSubmit->render(); ?> 
			</div>
			</form>
			
			<br/>
			
			<?php $class->table->render(); ?>
			
		</div>
	</div>
</div>