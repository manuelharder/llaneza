<!doctype html>
<!--[if IE 7]>         <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>         <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
<meta name="description" content="<?php echo $class->Description; ?>" />
<meta name="author" content="" />
<meta http-equiv="content-language" content="en" />
<meta name="keywords" content="<?php echo $class->Keywords; ?>" />
<meta name="robots" content="<?php echo $class->Robots; ?>" />
<link rel="shortcut icon" href="<?php echo IMAGES; ?>favicon.ico" type="image/x-icon" />
<title><?php echo $class->Title; ?></title>

<link rel="stylesheet" type="text/css" href="<?php echo CSS; ?>reset.css"/>
<link href="<?php echo CSS; ?>foundation.css" rel="stylesheet" type="text/css" />
<link href="<?php echo CSS; ?>styles.css" rel="stylesheet" type="text/css" />
	
<script type="text/javascript" src="<?php echo JS; ?>jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="<?php echo JS; ?>script.js"></script>

      
</head>
<body class="<?php echo $class->Structure->urlpath . " " . str_replace("/", " ", $class->Structure->folder); ?>">


<div id="wrap">
	<div id="main">

		<header>
			<div class="width">
				<a class="logo" href="/"></a>
				<nav>
					
				</nav>
			</div>	
		</header>