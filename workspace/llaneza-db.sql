-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 17, 2014 at 10:17 AM
-- Server version: 5.5.8
-- PHP Version: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `wm`
--

-- --------------------------------------------------------

--
-- Table structure for table `ll_page`
--

CREATE TABLE IF NOT EXISTS `ll_page` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `link` varchar(50) NOT NULL,
  `urlpath` varchar(50) NOT NULL,
  `folder` varchar(45) NOT NULL,
  `headline` varchar(90) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `ll_page`
--

INSERT INTO `ll_page` (`id`, `link`, `urlpath`, `folder`, `headline`) VALUES
(1, 'home.php', '/', '', ''),
(11, 'atable.php', 'atable', 'example', ''),
(12, 'ajaxform.php', 'ajaxform', 'example', ''),
(13, 'ajaxedpanel.php', 'ajaxedpanel', 'example', ''),
(14, 'selectboxtest.php', 'selectbox', 'example', '');
