<?php

class PageLink extends Link {

	
	public function __construct($objParent, $id, $target = "self", $ajax = "false") {
		
		parent::__construct($objParent);
				
		$this->strHref 		= "javascript:void(0)";
		$this->addCustomAttr("postAjax", "pageId=" . $id); 
		$this->strTarget 		= $target;
		$this->strLinkText 	= $id;
		$this->addCssClass("pageId");
	}
	
		
}

?>