<?php

class DeleteLink extends Link {

	
	public function __construct($objParent, $id, $linkText = "", $target = "self", $ajax = "false") {
		
		parent::__construct($objParent);
				
		$this->strHref 		= "javascript:void(0)";
		$this->addCustomAttr("postAjax", "deleteId=" . $id); 
		$this->strTarget 		= $target;
		$this->strLinkText 	= $linkText;
		$this->addCssClass("delete");
		
	}
	
		
}

?>