<?php

class ExampleMenu {

	
	public static function returnMenu() {
		
		$db = MyDB::getDB();
		
		$menuItems = $db->queryObjArray("SELECT * FROM ll_page WHERE folder = 'example'");
		
		$structure = Router::getStructure();
		
		$return = '<ul class="subnav">';
		
		foreach ($menuItems as $item) {
			
			$active = ($structure->urlpath == $item->urlpath) ? " class='active'" : "";
			
			$return .= '<li ' . $active . '><a href="/' . $item->folder . '/' . $item->urlpath . '"> ' . $item->urlpath . '</a></li>';	
		}
		
		return $return . '</ul>';
	}
	

		
}

?>