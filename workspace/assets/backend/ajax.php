<?php 

require_once '../../config/config.php';


if (!isset($_POST['sessionId'])) $_POST['sessionId'] = "wrong";

if (!isset($_SESSION['ll' . $_POST['sessionId']])) {
	
	echo "<div class='result'>";
	
	echo "<redirect>" . $_POST['url'] . "</redirect>";

	echo "</div>";

	exit();
}



$class = unserialize($_SESSION['ll' . $_POST['sessionId']]);

$ajaxBackendClass = new AjaxBackend($_POST);


echo "<div class='result'>";

echo $ajaxBackendClass->handleCall($class);

echo "</div>";

MyDB::closeDBConnection();
	
$_SESSION['ll' . $_POST['sessionId']] = serialize($class);


?>